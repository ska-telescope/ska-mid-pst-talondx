#ifndef CIRCUIT_SWITCH_H
#define CIRCUIT_SWITCH_H

#include <vector>
#include <string>

#include <algorithm>
#include <stdexcept>
#include <any>

#include "circuit_switch_regset.hpp"
#include "fpga_driver_base.hpp"

namespace fpga_driver {
    struct circuit_switch_param_t {
        uint8_t number_of_inputs = 1;
        uint8_t number_of_outputs = 1;
    };

    struct circuit_switch_config_t {
        uint8_t output = 255;
        uint8_t input = 255;
    };

    struct circuit_switch_status_t {
        uint8_t num_inputs;
        uint8_t num_outputs;
        std::vector<circuit_switch_config_t> connected;
    };

    class Circuit_Switch_Driver : public fpga_driver::Base<circuit_switch_config_t, circuit_switch_status_t>
    {
        public:
            Circuit_Switch_Driver (RegisterSetInfos &regsetinfos, Logger &logger) 
            : Base<circuit_switch_config_t,circuit_switch_status_t>(regsetinfos, logger), regsetinfos_(regsetinfos) {
                csr_params_ = regsetinfos_.at("csr").get_parameters<circuit_switch_param_t>();
                auto *csr_map_ = new RegisterMap<circuit_switch_regset::circuit_switch_reg_t, circuit_switch_regset::circuit_switch_mem_t>(get_regset_info("csr", circuit_switch_regset::version));
                csr_ = csr_map_->regs();

            };

            void setIdentity(const std::string &name, const std::string &address) {
                this->name = name;
                this->address = address;
            };

            ~Circuit_Switch_Driver () {
                csr_.reset();
            };

            virtual void get_default_config (circuit_switch_config_t &c) {
                c.output = csr_params_.number_of_outputs; // Will error if used.
                c.input = csr_params_.number_of_inputs; // number_of_inputs means disconnected
            };

        private:
            RegisterSetInfos regsetinfos_;
            std::shared_ptr<circuit_switch_regset::circuit_switch_reg_t> csr_ = nullptr;
            circuit_switch_param_t csr_params_;
            std::string name    = "";
            std::string address = "";

            virtual void configure_(const circuit_switch_config_t &c) {
                // Check if port you are trying to connect to exists
                if (c.input < 0  || c.input >= csr_params_.number_of_inputs ||
                    c.output < 0 || c.output >= csr_params_.number_of_outputs) {
                    throw std::out_of_range("Input or Output port value out of range. Input:"+std::to_string(c.input)+ ", Output: "+std::to_string(c.input)+". Max input value: "+
                    std::to_string(csr_params_.number_of_inputs - 1)+", Max output value: "+std::to_string(csr_params_.number_of_outputs - 1));
                }

                // Ensure input is not already connected
                if(csr_->input_select[c.output] == 255 || csr_->input_select[c.output] == csr_params_.number_of_inputs) {
                    csr_->input_select[c.output] = c.input;
                } else {
                    throw std::invalid_argument("Failed to connect Input '"+std::to_string(c.input)+"' to Output '"\
                    +std::to_string(c.output)+"', already connected to Input '"+std::to_string(csr_->input_select[c.output])+"'");
                }                
            };

            virtual void status_(circuit_switch_status_t &s, bool clear = false) {
                s.num_inputs = csr_params_.number_of_inputs;
                s.num_outputs = csr_params_.number_of_outputs;
                s.connected = std::vector<circuit_switch_config_t>(s.num_outputs,circuit_switch_config_t{255,255});

                for(int i = 0; i < s.num_outputs; i++) {
                    s.connected[i].input = csr_->input_select[i];
                    s.connected[i].output = static_cast<uint8_t>(i);
                }
            };

            virtual void recover_() {
                for(unsigned i = 0; i < csr_params_.number_of_outputs; i++) {
                    csr_->input_select[i] = csr_params_.number_of_inputs;
                }
            };
            
            virtual void deconfigure_(const circuit_switch_config_t &c) {
                if (c.output < 0 || c.output >= csr_params_.number_of_outputs) {
                    throw std::out_of_range("Output port value out of range. Output port: "+std::to_string(c.input)+", Max output value: "+std::to_string(csr_params_.number_of_outputs - 1));
                }
                csr_->input_select[c.output] = csr_params_.number_of_inputs;
            };

            virtual void start_() {};
            virtual void stop_(bool force = false) {};
    };
};
#endif // CIRCUIT_SWITCH_H
