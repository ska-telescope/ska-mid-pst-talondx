#ifndef WIDEBAND_BUFFER_H
#define WIDEBAND_BUFFER_H

#include <vector>
#include <string>

#include <algorithm>
#include <stdexcept>

#include "wideband_input_buffer_regset.hpp"
#include "fpga_driver_base.hpp"

namespace fpga_driver {
    struct wideband_input_buffer_param_t {
    };

    struct wideband_input_buffer_config_t {
        uint64_t expected_sample_rate;
        float noise_diode_transition_holdoff_seconds;
        uint8_t expected_dish_band;
    };

    struct wideband_input_buffer_status_t {
        bool buffer_underflowed;
        bool buffer_overflowed;
        bool error;

        uint32_t loss_of_signal_seconds;
        uint8_t meta_band_id;
        uint16_t meta_dish_id;
        uint32_t rx_sample_rate;
        uint64_t meta_transport_sample_rate;
    };

    class Wideband_Input_Buffer_Driver : public fpga_driver::Base<wideband_input_buffer_config_t, wideband_input_buffer_status_t>  
    {
        public:
            Wideband_Input_Buffer_Driver(wideband_input_buffer_param_t &params, RegisterSetInfos &regset, Logger &logger) 
            : Base<wideband_input_buffer_config_t,wideband_input_buffer_status_t>(regset, logger), params_(params) {
                log->info("Constructing wideband_input_buffer...");
                auto *reg_map = new RegisterMap<wideband_input_buffer_regset::wideband_input_buffer_reg_t>(get_regset_info("csr", wideband_input_buffer_regset::version));
                csr = reg_map->regs();
            };

            void setIdentity(const std::string &name, const std::string &address) {
                this->name = name;
                this->address = address;
            };

            ~Wideband_Input_Buffer_Driver() {
                log->info("Destructor wideband_input_buffer...");
                csr.reset();
            };

            virtual void get_default_config(wideband_input_buffer_config_t &c) {
                c.expected_sample_rate = 3960000000;
                c.noise_diode_transition_holdoff_seconds = 65535;
                c.expected_dish_band = 0;
            };

        private:
            RegisterMap<wideband_input_buffer_regset::wideband_input_buffer_reg_t> *reg_map;
            std::shared_ptr<wideband_input_buffer_regset::wideband_input_buffer_reg_t> csr = nullptr;
            wideband_input_buffer_param_t params_;
            std::string name    = "";
            std::string address = "";

            virtual void recover_() {
                log->info("Recovering wideband_input_buffer...");
                csr->packet_error = 1;
                csr->packet_drop = 1;
                csr->link_failure = 1;
                csr->buffer_overflow = 1;
            };

            virtual void configure_(const wideband_input_buffer_config_t &c) {
                log->info("configuring wideband_input_buffer ...");

                // Check firmware_band & expected_dish_band match, set preconfigured values
                uint32_t firmware_band = csr->firmware_band;
                uint8_t num_output_streams = 0;
                uint16_t packet_rate_value = 0;
                if (firmware_band == 3 && (c.expected_dish_band == 1 || c.expected_dish_band == 2)) {
                    num_output_streams = 18;
                    packet_rate_value  = 295;
                } else if (firmware_band == 3 && c.expected_dish_band == 3) {
                    num_output_streams = 18;
                    packet_rate_value  = 369;
                } else if (firmware_band == 4 && (c.expected_dish_band == 4)) {
                    num_output_streams = 27;
                    packet_rate_value  = 300;
                } else if (firmware_band == 5 && (c.expected_dish_band == 5 || c.expected_dish_band == 6)) {
                    num_output_streams = 54;
                    packet_rate_value  = 300;
                }
                if (num_output_streams == 0 || packet_rate_value == 0) {
                    throw std::invalid_argument("Wideband Input Buffer firmware configured for band "+std::to_string(firmware_band)
                    +", software expected it to be configured for band "+std::to_string(c.expected_dish_band));
                }

                // Confirm both calculation and config values are valid
                float stream_rate_calculation = (c.expected_sample_rate / num_output_streams); // Set as float to keep calculations consistent
                uint16_t noise_diode_calculation = static_cast<uint16_t> (65536 * c.noise_diode_transition_holdoff_seconds * stream_rate_calculation);
                if (noise_diode_calculation < 0 || noise_diode_calculation > 65535) {
                    throw std::out_of_range("Register - noise_diode_transition_holdoff_count: "+std::to_string(csr->noise_diode_transition_holdoff_count)
                    +" should be between [0,65535]");
                } else if (c.noise_diode_transition_holdoff_seconds < 0 || c.noise_diode_transition_holdoff_seconds > 65535) {
                    throw std::out_of_range("Config - noise_diode_transition_holdoff_count: "+std::to_string(c.noise_diode_transition_holdoff_seconds)
                    +" should be between [0,65535]");
                }
                
                csr->noise_diode_transition_holdoff_count = static_cast<uint16_t> (noise_diode_calculation);
                csr->stream_rate = static_cast<uint32_t> (stream_rate_calculation); // uint32_t to 28 bit register
                csr->packet_rate = static_cast<uint16_t> (packet_rate_value); // uint16_t to 9 bit register
            };

            virtual void start_() {
                log->info("Starting wideband_input_buffer...");
                csr->receive_enable = true;
            };

            virtual void stop_(bool force = false) {
                log->info("Stopping wideband_input_buffer...");
                csr->receive_enable = false;
            };

            virtual void deconfigure_(const wideband_input_buffer_config_t &c) {
                log->info("Deconfiguring wideband_input_buffer...");
            };

            virtual void status_(wideband_input_buffer_status_t &s, bool clear = false) {
                log->info("Fetching status wideband_input_buffer...");

                s.buffer_underflowed = csr->link_failure;
                s.buffer_overflowed = csr->buffer_overflow;
                
                s.loss_of_signal_seconds = csr->los_seconds;
                s.rx_sample_rate = csr->rx_sample_rate;

                s.meta_band_id = csr->meta_band_id;
                s.meta_dish_id = csr->meta_dish_id;

                if (csr->meta_transport_sample_rate_msw != 0) {
                    s.meta_transport_sample_rate = (uint64_t) csr->meta_transport_sample_rate_msw<<32 | csr->meta_transport_sample_rate_lsw;
                } else {
                    s.meta_transport_sample_rate = csr->meta_transport_sample_rate_lsw;
                }

                s.error = csr->packet_error == 1 || csr->packet_drop == 1 || csr->link_failure == 1 || csr->buffer_overflow == 1;
                if (clear) {
                    csr->packet_error = 1;
                    csr->packet_drop = 1;
                    csr->link_failure = 1;
                    csr->buffer_overflow = 1;
                }
            };       

    };
};
#endif // WIDEBAND_BUFFER_H