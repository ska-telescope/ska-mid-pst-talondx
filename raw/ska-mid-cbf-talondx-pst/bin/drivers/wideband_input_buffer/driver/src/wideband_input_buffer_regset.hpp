
#ifndef __WIDEBAND_INPUT_BUFFER_REGSET__H__
    #define __WIDEBAND_INPUT_BUFFER_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace wideband_input_buffer_regset {

        const std::string mnemonic = "wideband_input_buffer";
        const std::string version = "1.2.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _receive_enable_register_s
        {
            reg_t<std::uint32_t,is_reg,0,0> receive_enable; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _receive_enable_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _receive_enable_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _transport_link_fault_register_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> packet_error; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> packet_drop; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> link_failure; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,3,3> buffer_overflow; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _transport_link_fault_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _transport_link_fault_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _firmware_band_register_s
        {
            reg_t<std::uint32_t,is_reg,3,0> firmware_band; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _firmware_band_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _firmware_band_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _stream_rate_register_s
        {
            reg_t<std::uint32_t,is_reg,27,0> stream_rate; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _stream_rate_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _stream_rate_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _packet_rate_register_s
        {
            reg_t<std::uint32_t,is_reg,8,0> packet_rate; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _packet_rate_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _packet_rate_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _noise_diode_transition_holdoff_count_register_s
        {
            reg_t<std::uint32_t,is_reg,15,0> noise_diode_transition_holdoff_count; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _noise_diode_transition_holdoff_count_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _noise_diode_transition_holdoff_count_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _packet_error_count_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> packet_error_count; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _packet_error_count_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _packet_error_count_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _packet_drop_count_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> packet_drop_count; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _packet_drop_count_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _packet_drop_count_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _los_seconds_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> los_seconds; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _los_seconds_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _los_seconds_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _meta_ethertype_register_s
        {
            reg_t<std::uint32_t,is_reg,15,0> meta_ethertype; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _meta_ethertype_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _meta_ethertype_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _meta_dish_band_id_register_s
        {
            union {
                reg_t<std::uint32_t,is_reg,15,0> meta_dish_id; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,23,16> meta_band_id; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union _meta_dish_band_id_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _meta_dish_band_id_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _meta_utc_time_code_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> meta_utc_time_code; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _meta_utc_time_code_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _meta_utc_time_code_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _meta_transport_sample_rate_lsw_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> meta_transport_sample_rate_lsw; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _meta_transport_sample_rate_lsw_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _meta_transport_sample_rate_lsw_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _meta_transport_sample_rate_msw_register_s
        {
            reg_t<std::uint32_t,is_reg,15,0> meta_transport_sample_rate_msw; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _meta_transport_sample_rate_msw_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _meta_transport_sample_rate_msw_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _meta_hardware_source_id_lsw_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> meta_hardware_source_id_lsw; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _meta_hardware_source_id_lsw_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _meta_hardware_source_id_lsw_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _meta_hardware_source_id_msw_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> meta_hardware_source_id_msw; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _meta_hardware_source_id_msw_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _meta_hardware_source_id_msw_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_packet_rate_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_packet_rate; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rx_packet_rate_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_packet_rate_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_sample_rate_register_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_sample_rate; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rx_sample_rate_register_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_sample_rate_register_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) wideband_input_buffer_regs
        {
            _receive_enable_register_u<is_reg> receive_enable_register;
            _transport_link_fault_register_u<is_reg> transport_link_fault_register;
            _firmware_band_register_u<is_reg> firmware_band_register;
            _stream_rate_register_u<is_reg> stream_rate_register;
            _packet_rate_register_u<is_reg> packet_rate_register;
            _noise_diode_transition_holdoff_count_register_u<is_reg> noise_diode_transition_holdoff_count_register;
            _packet_error_count_register_u<is_reg> packet_error_count_register;
            _packet_drop_count_register_u<is_reg> packet_drop_count_register;
            _los_seconds_register_u<is_reg> los_seconds_register;
            _meta_ethertype_register_u<is_reg> meta_ethertype_register;
            _meta_dish_band_id_register_u<is_reg> meta_dish_band_id_register;
            _meta_utc_time_code_register_u<is_reg> meta_utc_time_code_register;
            _meta_transport_sample_rate_lsw_register_u<is_reg> meta_transport_sample_rate_lsw_register;
            _meta_transport_sample_rate_msw_register_u<is_reg> meta_transport_sample_rate_msw_register;
            _meta_hardware_source_id_lsw_register_u<is_reg> meta_hardware_source_id_lsw_register;
            _meta_hardware_source_id_msw_register_u<is_reg> meta_hardware_source_id_msw_register;
            _rx_packet_rate_register_u<is_reg> rx_packet_rate_register;
            _rx_sample_rate_register_u<is_reg> rx_sample_rate_register;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) wideband_input_buffer_fields
        {
            reg_t<std::uint32_t,is_reg,0,0> receive_enable; /*!< [rw] */
            union {
                reg_t<std::uint32_t,is_reg,0,0> packet_error; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> packet_drop; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> link_failure; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,3,3> buffer_overflow; /*!< [rw] */
            };
            reg_t<std::uint32_t,is_reg,3,0> firmware_band; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,27,0> stream_rate; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,8,0> packet_rate; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,15,0> noise_diode_transition_holdoff_count; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,31,0> packet_error_count; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> packet_drop_count; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> los_seconds; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,15,0> meta_ethertype; /*!< [ro] */
            union {
                reg_t<std::uint32_t,is_reg,15,0> meta_dish_id; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,23,16> meta_band_id; /*!< [ro] */
            };
            reg_t<std::uint32_t,is_reg,31,0> meta_utc_time_code; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> meta_transport_sample_rate_lsw; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,15,0> meta_transport_sample_rate_msw; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> meta_hardware_source_id_lsw; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> meta_hardware_source_id_msw; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rx_packet_rate; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rx_sample_rate; /*!< [ro] */
        };

        template<bool is_reg=false>
        union wideband_input_buffer_u
        {
            wideband_input_buffer_regs<is_reg> reg;
            wideband_input_buffer_fields<is_reg> fld;
        };

        typedef wideband_input_buffer_fields<true>  wideband_input_buffer_reg_t;
        typedef wideband_input_buffer_fields<false> wideband_input_buffer_mem_t;




        static_assert(4 == sizeof(_receive_enable_register_s<>), "struct _receive_enable_register_s does not have the correct size.");
        static_assert(4 == sizeof(_receive_enable_register_u<>), "union _receive_enable_register_u does not have the correct size.");
        static_assert(4 == sizeof(_transport_link_fault_register_s<>), "struct _transport_link_fault_register_s does not have the correct size.");
        static_assert(4 == sizeof(_transport_link_fault_register_u<>), "union _transport_link_fault_register_u does not have the correct size.");
        static_assert(4 == sizeof(_firmware_band_register_s<>), "struct _firmware_band_register_s does not have the correct size.");
        static_assert(4 == sizeof(_firmware_band_register_u<>), "union _firmware_band_register_u does not have the correct size.");
        static_assert(4 == sizeof(_stream_rate_register_s<>), "struct _stream_rate_register_s does not have the correct size.");
        static_assert(4 == sizeof(_stream_rate_register_u<>), "union _stream_rate_register_u does not have the correct size.");
        static_assert(4 == sizeof(_packet_rate_register_s<>), "struct _packet_rate_register_s does not have the correct size.");
        static_assert(4 == sizeof(_packet_rate_register_u<>), "union _packet_rate_register_u does not have the correct size.");
        static_assert(4 == sizeof(_noise_diode_transition_holdoff_count_register_s<>), "struct _noise_diode_transition_holdoff_count_register_s does not have the correct size.");
        static_assert(4 == sizeof(_noise_diode_transition_holdoff_count_register_u<>), "union _noise_diode_transition_holdoff_count_register_u does not have the correct size.");
        static_assert(4 == sizeof(_packet_error_count_register_s<>), "struct _packet_error_count_register_s does not have the correct size.");
        static_assert(4 == sizeof(_packet_error_count_register_u<>), "union _packet_error_count_register_u does not have the correct size.");
        static_assert(4 == sizeof(_packet_drop_count_register_s<>), "struct _packet_drop_count_register_s does not have the correct size.");
        static_assert(4 == sizeof(_packet_drop_count_register_u<>), "union _packet_drop_count_register_u does not have the correct size.");
        static_assert(4 == sizeof(_los_seconds_register_s<>), "struct _los_seconds_register_s does not have the correct size.");
        static_assert(4 == sizeof(_los_seconds_register_u<>), "union _los_seconds_register_u does not have the correct size.");
        static_assert(4 == sizeof(_meta_ethertype_register_s<>), "struct _meta_ethertype_register_s does not have the correct size.");
        static_assert(4 == sizeof(_meta_ethertype_register_u<>), "union _meta_ethertype_register_u does not have the correct size.");
        static_assert(4 == sizeof(_meta_dish_band_id_register_s<>), "struct _meta_dish_band_id_register_s does not have the correct size.");
        static_assert(4 == sizeof(_meta_dish_band_id_register_u<>), "union _meta_dish_band_id_register_u does not have the correct size.");
        static_assert(4 == sizeof(_meta_utc_time_code_register_s<>), "struct _meta_utc_time_code_register_s does not have the correct size.");
        static_assert(4 == sizeof(_meta_utc_time_code_register_u<>), "union _meta_utc_time_code_register_u does not have the correct size.");
        static_assert(4 == sizeof(_meta_transport_sample_rate_lsw_register_s<>), "struct _meta_transport_sample_rate_lsw_register_s does not have the correct size.");
        static_assert(4 == sizeof(_meta_transport_sample_rate_lsw_register_u<>), "union _meta_transport_sample_rate_lsw_register_u does not have the correct size.");
        static_assert(4 == sizeof(_meta_transport_sample_rate_msw_register_s<>), "struct _meta_transport_sample_rate_msw_register_s does not have the correct size.");
        static_assert(4 == sizeof(_meta_transport_sample_rate_msw_register_u<>), "union _meta_transport_sample_rate_msw_register_u does not have the correct size.");
        static_assert(4 == sizeof(_meta_hardware_source_id_lsw_register_s<>), "struct _meta_hardware_source_id_lsw_register_s does not have the correct size.");
        static_assert(4 == sizeof(_meta_hardware_source_id_lsw_register_u<>), "union _meta_hardware_source_id_lsw_register_u does not have the correct size.");
        static_assert(4 == sizeof(_meta_hardware_source_id_msw_register_s<>), "struct _meta_hardware_source_id_msw_register_s does not have the correct size.");
        static_assert(4 == sizeof(_meta_hardware_source_id_msw_register_u<>), "union _meta_hardware_source_id_msw_register_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_packet_rate_register_s<>), "struct _rx_packet_rate_register_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_packet_rate_register_u<>), "union _rx_packet_rate_register_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_sample_rate_register_s<>), "struct _rx_sample_rate_register_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_sample_rate_register_u<>), "union _rx_sample_rate_register_u does not have the correct size.");
        static_assert(72 == sizeof(wideband_input_buffer_regs<>), "struct wideband_input_buffer_regs does not have the correct size.");
        static_assert(72 == sizeof(wideband_input_buffer_fields<>), "struct wideband_input_buffer_fields does not have the correct size.");
        static_assert(72 == sizeof(wideband_input_buffer_u<>), "union wideband_input_buffer_u does not have the correct size.");
        static_assert(72 == sizeof(wideband_input_buffer_reg_t), "type wideband_input_buffer_reg_t does not have the correct size.");
        static_assert(72 == sizeof(wideband_input_buffer_mem_t), "type wideband_input_buffer_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
