#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "src/wideband_input_buffer_driver.hpp"

namespace py = pybind11;
using namespace fpga_driver;

FPGA_DRIVER_PYBIND_MODULE_DECLARATION(wideband_input_buffer, m) {
    py::class_<wideband_input_buffer_param_t>(m, "param_t")
        .def(py::init<>());

    py::class_<wideband_input_buffer_config_t>(m, "config_t")
        .def(py::init<>())
        .def(py::init<uint64_t,float,uint8_t>())
        .def_readwrite("expected_sample_rate", &wideband_input_buffer_config_t::expected_sample_rate)
        .def_readwrite("noise_diode_transition_holdoff_seconds", &wideband_input_buffer_config_t::noise_diode_transition_holdoff_seconds)
        .def_readwrite("expected_dish_band", &wideband_input_buffer_config_t::expected_dish_band);

    py::class_<wideband_input_buffer_status_t>(m, "status_t")
        .def(py::init<>())
        .def_readwrite("buffer_underflowed", &wideband_input_buffer_status_t::buffer_underflowed)
        .def_readwrite("buffer_overflowed", &wideband_input_buffer_status_t::buffer_overflowed)
        .def_readwrite("error", &wideband_input_buffer_status_t::error)
        .def_readwrite("loss_of_signal_seconds", &wideband_input_buffer_status_t::loss_of_signal_seconds)
        .def_readwrite("meta_band_id", &wideband_input_buffer_status_t::meta_band_id)
        .def_readwrite("meta_dish_id", &wideband_input_buffer_status_t::meta_dish_id)
        .def_readwrite("rx_sample_rate", &wideband_input_buffer_status_t::rx_sample_rate)
        .def_readwrite("meta_transport_sample_rate", &wideband_input_buffer_status_t::meta_transport_sample_rate);

    py::class_<Wideband_Input_Buffer_Driver>(m, "driver")
        .def(py::init<wideband_input_buffer_param_t&, RegisterSetInfos&, Logger&>())
        .def("recover", &Wideband_Input_Buffer_Driver::recover)
        .def("configure", &Wideband_Input_Buffer_Driver::configure)
        .def("deconfigure", &Wideband_Input_Buffer_Driver::deconfigure)
        .def("start", &Wideband_Input_Buffer_Driver::start)
        .def("stop", &Wideband_Input_Buffer_Driver::stop)
        .def("status", &Wideband_Input_Buffer_Driver::status)
        .def("get_default_config", &Wideband_Input_Buffer_Driver::get_default_config);
}