
#ifndef __ETHERNET_100G_RX_MAC_REGSET__H__
    #define __ETHERNET_100G_RX_MAC_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace Ethernet_100G_rx_MAC_regset {

        const std::string mnemonic = "Ethernet_100G_rx_MAC";
        const std::string version = "19.2.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxmac_revid_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rxmac_revid; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rxmac_revid_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rxmac_revid_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxmac_scratch_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rxmac_scratch; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _rxmac_scratch_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rxmac_scratch_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxmac_name_0_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rxmac_name_0; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rxmac_name_0_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rxmac_name_0_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxmac_name_1_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rxmac_name_1; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rxmac_name_1_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rxmac_name_1_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxmac_name_2_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rxmac_name_2; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rxmac_name_2_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rxmac_name_2_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxmac_size_config_s
        {
            reg_t<std::uint32_t,is_reg,15,0> rxmac_size_config; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _rxmac_size_config_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rxmac_size_config_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _mac_crc_config_s
        {
            reg_t<std::uint32_t,is_reg,0,0> mac_crc_config; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _mac_crc_config_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _mac_crc_config_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_link_fault_s
        {
            reg_t<std::uint32_t,is_reg,1,0> link_fault; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rx_link_fault_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_link_fault_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxmac_control_s
        {
            union {
                reg_t<std::uint32_t,is_reg,1,1> rx_vlan_detect_disable; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,3,3> sfd_check; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> preamble_check; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _rxmac_control_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rxmac_control_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_rx_MAC_regs
        {
            _rxmac_revid_u<is_reg> rxmac_revid;
            _rxmac_scratch_u<is_reg> rxmac_scratch;
            _rxmac_name_0_u<is_reg> rxmac_name_0;
            _rxmac_name_1_u<is_reg> rxmac_name_1;
            _rxmac_name_2_u<is_reg> rxmac_name_2;
            reg_t<std::uint8_t,is_reg> reserved1[4];
            _rxmac_size_config_u<is_reg> rxmac_size_config;
            _mac_crc_config_u<is_reg> mac_crc_config;
            _rx_link_fault_u<is_reg> rx_link_fault;
            reg_t<std::uint8_t,is_reg> reserved3[4];
            _rxmac_control_u<is_reg> rxmac_control;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_rx_MAC_fields
        {
            reg_t<std::uint32_t,is_reg,31,0> rxmac_revid; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rxmac_scratch; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,31,0> rxmac_name_0; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rxmac_name_1; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rxmac_name_2; /*!< [ro] */
            reg_t<std::uint8_t,is_reg> reserved5[4];
            reg_t<std::uint32_t,is_reg,15,0> rxmac_size_config; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,0,0> mac_crc_config; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,1,0> link_fault; /*!< [ro] */
            reg_t<std::uint8_t,is_reg> reserved7[4];
            union {
                reg_t<std::uint32_t,is_reg,1,1> rx_vlan_detect_disable; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,3,3> sfd_check; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> preamble_check; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union Ethernet_100G_rx_MAC_u
        {
            Ethernet_100G_rx_MAC_regs<is_reg> reg;
            Ethernet_100G_rx_MAC_fields<is_reg> fld;
        };

        typedef Ethernet_100G_rx_MAC_fields<true>  Ethernet_100G_rx_MAC_reg_t;
        typedef Ethernet_100G_rx_MAC_fields<false> Ethernet_100G_rx_MAC_mem_t;




        static_assert(4 == sizeof(_rxmac_revid_s<>), "struct _rxmac_revid_s does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_revid_u<>), "union _rxmac_revid_u does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_scratch_s<>), "struct _rxmac_scratch_s does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_scratch_u<>), "union _rxmac_scratch_u does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_name_0_s<>), "struct _rxmac_name_0_s does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_name_0_u<>), "union _rxmac_name_0_u does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_name_1_s<>), "struct _rxmac_name_1_s does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_name_1_u<>), "union _rxmac_name_1_u does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_name_2_s<>), "struct _rxmac_name_2_s does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_name_2_u<>), "union _rxmac_name_2_u does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_size_config_s<>), "struct _rxmac_size_config_s does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_size_config_u<>), "union _rxmac_size_config_u does not have the correct size.");
        static_assert(4 == sizeof(_mac_crc_config_s<>), "struct _mac_crc_config_s does not have the correct size.");
        static_assert(4 == sizeof(_mac_crc_config_u<>), "union _mac_crc_config_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_link_fault_s<>), "struct _rx_link_fault_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_link_fault_u<>), "union _rx_link_fault_u does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_control_s<>), "struct _rxmac_control_s does not have the correct size.");
        static_assert(4 == sizeof(_rxmac_control_u<>), "union _rxmac_control_u does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_rx_MAC_regs<>), "struct Ethernet_100G_rx_MAC_regs does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_rx_MAC_fields<>), "struct Ethernet_100G_rx_MAC_fields does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_rx_MAC_u<>), "union Ethernet_100G_rx_MAC_u does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_rx_MAC_reg_t), "type Ethernet_100G_rx_MAC_reg_t does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_rx_MAC_mem_t), "type Ethernet_100G_rx_MAC_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
