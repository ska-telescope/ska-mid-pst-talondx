
#ifndef __ETHERNET_100G_TX_MAC_REGSET__H__
    #define __ETHERNET_100G_TX_MAC_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace Ethernet_100G_tx_MAC_regset {

        const std::string mnemonic = "Ethernet_100G_tx_MAC";
        const std::string version = "19.2.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _txmac_revid_s
        {
            reg_t<std::uint32_t,is_reg,31,0> txmac_revid; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _txmac_revid_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _txmac_revid_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _txmac_scratch_s
        {
            reg_t<std::uint32_t,is_reg,31,0> txmac_scratch; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _txmac_scratch_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _txmac_scratch_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _txmac_name_0_s
        {
            reg_t<std::uint32_t,is_reg,31,0> txmac_name_0; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _txmac_name_0_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _txmac_name_0_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _txmac_name_1_s
        {
            reg_t<std::uint32_t,is_reg,31,0> txmac_name_1; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _txmac_name_1_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _txmac_name_1_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _txmac_name_2_s
        {
            reg_t<std::uint32_t,is_reg,31,0> txmac_name_2; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _txmac_name_2_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _txmac_name_2_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _tx_link_fault_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> pcs_gen_fault_sequence; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> unidir_enable; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> disable_remote_fault; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,3,3> force_remote_fault; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _tx_link_fault_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _tx_link_fault_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _ipg_col_rem_s
        {
            reg_t<std::uint32_t,is_reg,6,0> ipg_col_rem; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _ipg_col_rem_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _ipg_col_rem_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _txmac_size_config_s
        {
            reg_t<std::uint32_t,is_reg,15,0> max_tx_size_config; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _txmac_size_config_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _txmac_size_config_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _tx_mac_control_s
        {
            reg_t<std::uint32_t,is_reg,1,1> tx_vlan_detect_disable; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _tx_mac_control_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _tx_mac_control_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_tx_MAC_regs
        {
            _txmac_revid_u<is_reg> txmac_revid;
            _txmac_scratch_u<is_reg> txmac_scratch;
            _txmac_name_0_u<is_reg> txmac_name_0;
            _txmac_name_1_u<is_reg> txmac_name_1;
            _txmac_name_2_u<is_reg> txmac_name_2;
            _tx_link_fault_u<is_reg> tx_link_fault;
            _ipg_col_rem_u<is_reg> ipg_col_rem;
            _txmac_size_config_u<is_reg> txmac_size_config;
            reg_t<std::uint8_t,is_reg> reserved1[8];
            _tx_mac_control_u<is_reg> tx_mac_control;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_tx_MAC_fields
        {
            reg_t<std::uint32_t,is_reg,31,0> txmac_revid; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> txmac_scratch; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,31,0> txmac_name_0; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> txmac_name_1; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> txmac_name_2; /*!< [ro] */
            union {
                reg_t<std::uint32_t,is_reg,0,0> pcs_gen_fault_sequence; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> unidir_enable; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> disable_remote_fault; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,3,3> force_remote_fault; /*!< [rw] */
            };
            reg_t<std::uint32_t,is_reg,6,0> ipg_col_rem; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,15,0> max_tx_size_config; /*!< [rw] */
            reg_t<std::uint8_t,is_reg> reserved3[8];
            reg_t<std::uint32_t,is_reg,1,1> tx_vlan_detect_disable; /*!< [rw] */
        };

        template<bool is_reg=false>
        union Ethernet_100G_tx_MAC_u
        {
            Ethernet_100G_tx_MAC_regs<is_reg> reg;
            Ethernet_100G_tx_MAC_fields<is_reg> fld;
        };

        typedef Ethernet_100G_tx_MAC_fields<true>  Ethernet_100G_tx_MAC_reg_t;
        typedef Ethernet_100G_tx_MAC_fields<false> Ethernet_100G_tx_MAC_mem_t;




        static_assert(4 == sizeof(_txmac_revid_s<>), "struct _txmac_revid_s does not have the correct size.");
        static_assert(4 == sizeof(_txmac_revid_u<>), "union _txmac_revid_u does not have the correct size.");
        static_assert(4 == sizeof(_txmac_scratch_s<>), "struct _txmac_scratch_s does not have the correct size.");
        static_assert(4 == sizeof(_txmac_scratch_u<>), "union _txmac_scratch_u does not have the correct size.");
        static_assert(4 == sizeof(_txmac_name_0_s<>), "struct _txmac_name_0_s does not have the correct size.");
        static_assert(4 == sizeof(_txmac_name_0_u<>), "union _txmac_name_0_u does not have the correct size.");
        static_assert(4 == sizeof(_txmac_name_1_s<>), "struct _txmac_name_1_s does not have the correct size.");
        static_assert(4 == sizeof(_txmac_name_1_u<>), "union _txmac_name_1_u does not have the correct size.");
        static_assert(4 == sizeof(_txmac_name_2_s<>), "struct _txmac_name_2_s does not have the correct size.");
        static_assert(4 == sizeof(_txmac_name_2_u<>), "union _txmac_name_2_u does not have the correct size.");
        static_assert(4 == sizeof(_tx_link_fault_s<>), "struct _tx_link_fault_s does not have the correct size.");
        static_assert(4 == sizeof(_tx_link_fault_u<>), "union _tx_link_fault_u does not have the correct size.");
        static_assert(4 == sizeof(_ipg_col_rem_s<>), "struct _ipg_col_rem_s does not have the correct size.");
        static_assert(4 == sizeof(_ipg_col_rem_u<>), "union _ipg_col_rem_u does not have the correct size.");
        static_assert(4 == sizeof(_txmac_size_config_s<>), "struct _txmac_size_config_s does not have the correct size.");
        static_assert(4 == sizeof(_txmac_size_config_u<>), "union _txmac_size_config_u does not have the correct size.");
        static_assert(4 == sizeof(_tx_mac_control_s<>), "struct _tx_mac_control_s does not have the correct size.");
        static_assert(4 == sizeof(_tx_mac_control_u<>), "union _tx_mac_control_u does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_tx_MAC_regs<>), "struct Ethernet_100G_tx_MAC_regs does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_tx_MAC_fields<>), "struct Ethernet_100G_tx_MAC_fields does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_tx_MAC_u<>), "union Ethernet_100G_tx_MAC_u does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_tx_MAC_reg_t), "type Ethernet_100G_tx_MAC_reg_t does not have the correct size.");
        static_assert(44 == sizeof(Ethernet_100G_tx_MAC_mem_t), "type Ethernet_100G_tx_MAC_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
