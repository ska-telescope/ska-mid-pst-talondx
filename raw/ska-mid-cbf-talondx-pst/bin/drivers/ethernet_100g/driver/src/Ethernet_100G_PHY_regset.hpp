
#ifndef __ETHERNET_100G_PHY_REGSET__H__
    #define __ETHERNET_100G_PHY_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace Ethernet_100G_PHY_regset {

        const std::string mnemonic = "Ethernet_100G_PHY";
        const std::string version = "19.2.1";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _revid_s
        {
            reg_t<std::uint32_t,is_reg,31,0> REVID; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _revid_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _revid_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _scratch_s
        {
            reg_t<std::uint32_t,is_reg,31,0> scratch; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _scratch_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _scratch_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _phy_name_0_s
        {
            reg_t<std::uint32_t,is_reg,31,0> phy_name_0; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _phy_name_0_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _phy_name_0_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _phy_name_1_s
        {
            reg_t<std::uint32_t,is_reg,31,0> phy_name_1; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _phy_name_1_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _phy_name_1_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _phy_name_2_s
        {
            reg_t<std::uint32_t,is_reg,31,0> phy_name_2; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _phy_name_2_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _phy_name_2_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _phy_config_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> eio_sys_rst; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> soft_txp_rst; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> soft_rxp_rst; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> set_ref_lock; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,5,5> set_data_lock; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _phy_config_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _phy_config_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _word_lock_s
        {
            reg_t<std::uint32_t,is_reg,19,0> word_lock; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _word_lock_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _word_lock_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _eio_sloop_s
        {
            reg_t<std::uint32_t,is_reg,3,0> eio_sloop; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _eio_sloop_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _eio_sloop_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _eio_flag_sel_s
        {
            reg_t<std::uint32_t,is_reg,2,0> eio_flag_sel; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _eio_flag_sel_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _eio_flag_sel_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _eio_flags_s
        {
            reg_t<std::uint32_t,is_reg,3,0> eio_flags; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _eio_flags_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _eio_flags_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _eio_freq_lock_s
        {
            reg_t<std::uint32_t,is_reg,3,0> eio_freq_lock; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _eio_freq_lock_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _eio_freq_lock_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _phy_clk_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> tx_reset_done; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> tx_core_clock_stable; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,2,2> rx_core_clock_stable; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union _phy_clk_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _phy_clk_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _frm_err_s
        {
            reg_t<std::uint32_t,is_reg,19,0> frm_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _frm_err_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _frm_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _sclr_frm_err_s
        {
            reg_t<std::uint32_t,is_reg,31,0> sclr_frm_err; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _sclr_frm_err_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _sclr_frm_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _EIO_RX_SOFT_PURGE_S_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> clear_rx_fifo; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,11,11> disable_bitslip; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,12,12> disable_autoadaption; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _EIO_RX_SOFT_PURGE_S_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _EIO_RX_SOFT_PURGE_S_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _RX_PCS_FULLY_ALIGNED_S_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> rx_pcs_fully_aligned; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> RX_PCS_HI_BER; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union _RX_PCS_FULLY_ALIGNED_S_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _RX_PCS_FULLY_ALIGNED_S_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _err_inj_s
        {
            reg_t<std::uint32_t,is_reg,3,0> err_inj; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _err_inj_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _err_inj_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _am_lock_s
        {
            reg_t<std::uint32_t,is_reg,0,0> am_lock; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _am_lock_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _am_lock_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _lane_deskewed_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> all_lanes_deskewed; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> deskew_change; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union _lane_deskewed_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _lane_deskewed_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _pcs_vlane_0_to_4_s
        {
            reg_t<std::uint32_t,is_reg,24,0> vlane_a; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _pcs_vlane_0_to_4_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _pcs_vlane_0_to_4_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _pcs_vlane_5_to_9_s
        {
            reg_t<std::uint32_t,is_reg,24,0> vlane_b; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _pcs_vlane_5_to_9_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _pcs_vlane_5_to_9_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _pcs_vlane_10_to_14_s
        {
            reg_t<std::uint32_t,is_reg,24,0> vlane_c; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _pcs_vlane_10_to_14_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _pcs_vlane_10_to_14_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _pcs_vlane_15_to_19_s
        {
            reg_t<std::uint32_t,is_reg,24,0> vlane_d; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _pcs_vlane_15_to_19_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _pcs_vlane_15_to_19_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _khz_ref_s
        {
            reg_t<std::uint32_t,is_reg,31,0> khz_ref; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _khz_ref_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _khz_ref_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _khz_rx_s
        {
            reg_t<std::uint32_t,is_reg,31,0> khz_rx; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _khz_rx_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _khz_rx_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _khz_tx_s
        {
            reg_t<std::uint32_t,is_reg,31,0> khz_tx; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _khz_tx_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _khz_tx_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _khz_tx_rs_s
        {
            reg_t<std::uint32_t,is_reg,31,0> khz_tx_rs; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _khz_tx_rs_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _khz_tx_rs_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _khz_rx_rs_s
        {
            reg_t<std::uint32_t,is_reg,31,0> khz_rx_rs; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _khz_rx_rs_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _khz_rx_rs_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _enable_rsfec_s
        {
            reg_t<std::uint32_t,is_reg,31,0> enable_rsfec; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _enable_rsfec_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _enable_rsfec_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_PHY_regs
        {
            _revid_u<is_reg> revid;
            _scratch_u<is_reg> scratch;
            _phy_name_0_u<is_reg> phy_name_0;
            _phy_name_1_u<is_reg> phy_name_1;
            _phy_name_2_u<is_reg> phy_name_2;
            reg_t<std::uint8_t,is_reg> reserved1[44];
            _phy_config_u<is_reg> phy_config;
            reg_t<std::uint8_t,is_reg> reserved3[4];
            _word_lock_u<is_reg> word_lock;
            _eio_sloop_u<is_reg> eio_sloop;
            _eio_flag_sel_u<is_reg> eio_flag_sel;
            _eio_flags_u<is_reg> eio_flags;
            reg_t<std::uint8_t,is_reg> reserved5[44];
            _eio_freq_lock_u<is_reg> eio_freq_lock;
            _phy_clk_u<is_reg> phy_clk;
            _frm_err_u<is_reg> frm_err;
            _sclr_frm_err_u<is_reg> sclr_frm_err;
            _EIO_RX_SOFT_PURGE_S_u<is_reg> EIO_RX_SOFT_PURGE_S;
            _RX_PCS_FULLY_ALIGNED_S_u<is_reg> RX_PCS_FULLY_ALIGNED_S;
            _err_inj_u<is_reg> err_inj;
            _am_lock_u<is_reg> am_lock;
            _lane_deskewed_u<is_reg> lane_deskewed;
            reg_t<std::uint8_t,is_reg> reserved7[24];
            _pcs_vlane_0_to_4_u<is_reg> pcs_vlane_0_to_4;
            _pcs_vlane_5_to_9_u<is_reg> pcs_vlane_5_to_9;
            _pcs_vlane_10_to_14_u<is_reg> pcs_vlane_10_to_14;
            _pcs_vlane_15_to_19_u<is_reg> pcs_vlane_15_to_19;
            reg_t<std::uint8_t,is_reg> reserved9[48];
            _khz_ref_u<is_reg> khz_ref;
            _khz_rx_u<is_reg> khz_rx;
            _khz_tx_u<is_reg> khz_tx;
            _khz_tx_rs_u<is_reg> khz_tx_rs;
            _khz_rx_rs_u<is_reg> khz_rx_rs;
            reg_t<std::uint8_t,is_reg> reserved11[44];
            _enable_rsfec_u<is_reg> enable_rsfec;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_PHY_fields
        {
            reg_t<std::uint32_t,is_reg,31,0> REVID; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> scratch; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,31,0> phy_name_0; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> phy_name_1; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> phy_name_2; /*!< [ro] */
            reg_t<std::uint8_t,is_reg> reserved13[44];
            union {
                reg_t<std::uint32_t,is_reg,0,0> eio_sys_rst; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> soft_txp_rst; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> soft_rxp_rst; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> set_ref_lock; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,5,5> set_data_lock; /*!< [rw] */
            };
            reg_t<std::uint8_t,is_reg> reserved15[4];
            reg_t<std::uint32_t,is_reg,19,0> word_lock; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,3,0> eio_sloop; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,2,0> eio_flag_sel; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,3,0> eio_flags; /*!< [rw] */
            reg_t<std::uint8_t,is_reg> reserved17[44];
            reg_t<std::uint32_t,is_reg,3,0> eio_freq_lock; /*!< [ro] */
            union {
                reg_t<std::uint32_t,is_reg,0,0> tx_reset_done; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> tx_core_clock_stable; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,2,2> rx_core_clock_stable; /*!< [ro] */
            };
            reg_t<std::uint32_t,is_reg,19,0> frm_err; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> sclr_frm_err; /*!< [rw] */
            union {
                reg_t<std::uint32_t,is_reg,0,0> clear_rx_fifo; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,11,11> disable_bitslip; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,12,12> disable_autoadaption; /*!< [rw] */
            };
            union {
                reg_t<std::uint32_t,is_reg,0,0> rx_pcs_fully_aligned; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> RX_PCS_HI_BER; /*!< [ro] */
            };
            reg_t<std::uint32_t,is_reg,3,0> err_inj; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,0,0> am_lock; /*!< [rw] */
            union {
                reg_t<std::uint32_t,is_reg,0,0> all_lanes_deskewed; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> deskew_change; /*!< [ro] */
            };
            reg_t<std::uint8_t,is_reg> reserved19[24];
            reg_t<std::uint32_t,is_reg,24,0> vlane_a; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,24,0> vlane_b; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,24,0> vlane_c; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,24,0> vlane_d; /*!< [ro] */
            reg_t<std::uint8_t,is_reg> reserved21[48];
            reg_t<std::uint32_t,is_reg,31,0> khz_ref; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> khz_rx; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> khz_tx; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> khz_tx_rs; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> khz_rx_rs; /*!< [ro] */
            reg_t<std::uint8_t,is_reg> reserved23[44];
            reg_t<std::uint32_t,is_reg,31,0> enable_rsfec; /*!< [rw] */
        };

        template<bool is_reg=false>
        union Ethernet_100G_PHY_u
        {
            Ethernet_100G_PHY_regs<is_reg> reg;
            Ethernet_100G_PHY_fields<is_reg> fld;
        };

        typedef Ethernet_100G_PHY_fields<true>  Ethernet_100G_PHY_reg_t;
        typedef Ethernet_100G_PHY_fields<false> Ethernet_100G_PHY_mem_t;




        static_assert(4 == sizeof(_revid_s<>), "struct _revid_s does not have the correct size.");
        static_assert(4 == sizeof(_revid_u<>), "union _revid_u does not have the correct size.");
        static_assert(4 == sizeof(_scratch_s<>), "struct _scratch_s does not have the correct size.");
        static_assert(4 == sizeof(_scratch_u<>), "union _scratch_u does not have the correct size.");
        static_assert(4 == sizeof(_phy_name_0_s<>), "struct _phy_name_0_s does not have the correct size.");
        static_assert(4 == sizeof(_phy_name_0_u<>), "union _phy_name_0_u does not have the correct size.");
        static_assert(4 == sizeof(_phy_name_1_s<>), "struct _phy_name_1_s does not have the correct size.");
        static_assert(4 == sizeof(_phy_name_1_u<>), "union _phy_name_1_u does not have the correct size.");
        static_assert(4 == sizeof(_phy_name_2_s<>), "struct _phy_name_2_s does not have the correct size.");
        static_assert(4 == sizeof(_phy_name_2_u<>), "union _phy_name_2_u does not have the correct size.");
        static_assert(4 == sizeof(_phy_config_s<>), "struct _phy_config_s does not have the correct size.");
        static_assert(4 == sizeof(_phy_config_u<>), "union _phy_config_u does not have the correct size.");
        static_assert(4 == sizeof(_word_lock_s<>), "struct _word_lock_s does not have the correct size.");
        static_assert(4 == sizeof(_word_lock_u<>), "union _word_lock_u does not have the correct size.");
        static_assert(4 == sizeof(_eio_sloop_s<>), "struct _eio_sloop_s does not have the correct size.");
        static_assert(4 == sizeof(_eio_sloop_u<>), "union _eio_sloop_u does not have the correct size.");
        static_assert(4 == sizeof(_eio_flag_sel_s<>), "struct _eio_flag_sel_s does not have the correct size.");
        static_assert(4 == sizeof(_eio_flag_sel_u<>), "union _eio_flag_sel_u does not have the correct size.");
        static_assert(4 == sizeof(_eio_flags_s<>), "struct _eio_flags_s does not have the correct size.");
        static_assert(4 == sizeof(_eio_flags_u<>), "union _eio_flags_u does not have the correct size.");
        static_assert(4 == sizeof(_eio_freq_lock_s<>), "struct _eio_freq_lock_s does not have the correct size.");
        static_assert(4 == sizeof(_eio_freq_lock_u<>), "union _eio_freq_lock_u does not have the correct size.");
        static_assert(4 == sizeof(_phy_clk_s<>), "struct _phy_clk_s does not have the correct size.");
        static_assert(4 == sizeof(_phy_clk_u<>), "union _phy_clk_u does not have the correct size.");
        static_assert(4 == sizeof(_frm_err_s<>), "struct _frm_err_s does not have the correct size.");
        static_assert(4 == sizeof(_frm_err_u<>), "union _frm_err_u does not have the correct size.");
        static_assert(4 == sizeof(_sclr_frm_err_s<>), "struct _sclr_frm_err_s does not have the correct size.");
        static_assert(4 == sizeof(_sclr_frm_err_u<>), "union _sclr_frm_err_u does not have the correct size.");
        static_assert(4 == sizeof(_EIO_RX_SOFT_PURGE_S_s<>), "struct _EIO_RX_SOFT_PURGE_S_s does not have the correct size.");
        static_assert(4 == sizeof(_EIO_RX_SOFT_PURGE_S_u<>), "union _EIO_RX_SOFT_PURGE_S_u does not have the correct size.");
        static_assert(4 == sizeof(_RX_PCS_FULLY_ALIGNED_S_s<>), "struct _RX_PCS_FULLY_ALIGNED_S_s does not have the correct size.");
        static_assert(4 == sizeof(_RX_PCS_FULLY_ALIGNED_S_u<>), "union _RX_PCS_FULLY_ALIGNED_S_u does not have the correct size.");
        static_assert(4 == sizeof(_err_inj_s<>), "struct _err_inj_s does not have the correct size.");
        static_assert(4 == sizeof(_err_inj_u<>), "union _err_inj_u does not have the correct size.");
        static_assert(4 == sizeof(_am_lock_s<>), "struct _am_lock_s does not have the correct size.");
        static_assert(4 == sizeof(_am_lock_u<>), "union _am_lock_u does not have the correct size.");
        static_assert(4 == sizeof(_lane_deskewed_s<>), "struct _lane_deskewed_s does not have the correct size.");
        static_assert(4 == sizeof(_lane_deskewed_u<>), "union _lane_deskewed_u does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_0_to_4_s<>), "struct _pcs_vlane_0_to_4_s does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_0_to_4_u<>), "union _pcs_vlane_0_to_4_u does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_5_to_9_s<>), "struct _pcs_vlane_5_to_9_s does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_5_to_9_u<>), "union _pcs_vlane_5_to_9_u does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_10_to_14_s<>), "struct _pcs_vlane_10_to_14_s does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_10_to_14_u<>), "union _pcs_vlane_10_to_14_u does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_15_to_19_s<>), "struct _pcs_vlane_15_to_19_s does not have the correct size.");
        static_assert(4 == sizeof(_pcs_vlane_15_to_19_u<>), "union _pcs_vlane_15_to_19_u does not have the correct size.");
        static_assert(4 == sizeof(_khz_ref_s<>), "struct _khz_ref_s does not have the correct size.");
        static_assert(4 == sizeof(_khz_ref_u<>), "union _khz_ref_u does not have the correct size.");
        static_assert(4 == sizeof(_khz_rx_s<>), "struct _khz_rx_s does not have the correct size.");
        static_assert(4 == sizeof(_khz_rx_u<>), "union _khz_rx_u does not have the correct size.");
        static_assert(4 == sizeof(_khz_tx_s<>), "struct _khz_tx_s does not have the correct size.");
        static_assert(4 == sizeof(_khz_tx_u<>), "union _khz_tx_u does not have the correct size.");
        static_assert(4 == sizeof(_khz_tx_rs_s<>), "struct _khz_tx_rs_s does not have the correct size.");
        static_assert(4 == sizeof(_khz_tx_rs_u<>), "union _khz_tx_rs_u does not have the correct size.");
        static_assert(4 == sizeof(_khz_rx_rs_s<>), "struct _khz_rx_rs_s does not have the correct size.");
        static_assert(4 == sizeof(_khz_rx_rs_u<>), "union _khz_rx_rs_u does not have the correct size.");
        static_assert(4 == sizeof(_enable_rsfec_s<>), "struct _enable_rsfec_s does not have the correct size.");
        static_assert(4 == sizeof(_enable_rsfec_u<>), "union _enable_rsfec_u does not have the correct size.");
        static_assert(324 == sizeof(Ethernet_100G_PHY_regs<>), "struct Ethernet_100G_PHY_regs does not have the correct size.");
        static_assert(324 == sizeof(Ethernet_100G_PHY_fields<>), "struct Ethernet_100G_PHY_fields does not have the correct size.");
        static_assert(324 == sizeof(Ethernet_100G_PHY_u<>), "union Ethernet_100G_PHY_u does not have the correct size.");
        static_assert(324 == sizeof(Ethernet_100G_PHY_reg_t), "type Ethernet_100G_PHY_reg_t does not have the correct size.");
        static_assert(324 == sizeof(Ethernet_100G_PHY_mem_t), "type Ethernet_100G_PHY_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
