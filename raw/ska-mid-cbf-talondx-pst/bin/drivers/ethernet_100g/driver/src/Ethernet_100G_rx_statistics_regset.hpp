
#ifndef __ETHERNET_100G_RX_STATISTICS_REGSET__H__
    #define __ETHERNET_100G_RX_STATISTICS_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace Ethernet_100G_rx_statistics_regset {

        const std::string mnemonic = "Ethernet_100G_rx_statistics";
        const std::string version = "19.2.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_fragments_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_fragments; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_fragments_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_fragments_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_jabbers_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_jabbers; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_jabbers_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_jabbers_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_fcs_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_fcs; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_fcs_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_fcs_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_crcerr_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_crcerr; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_crcerr_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_crcerr_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_mcast_data_err_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_data_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_mcast_data_err_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_mcast_data_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_bcast_data_err_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_data_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_bcast_data_err_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_bcast_data_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_ucast_data_err_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_data_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_ucast_data_err_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_ucast_data_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_mcast_ctrl_err_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_ctrl_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_mcast_ctrl_err_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_mcast_ctrl_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_bcast_ctrl_err_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_ctrl_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_bcast_ctrl_err_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_bcast_ctrl_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_ucast_ctrl_err_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_ctrl_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_ucast_ctrl_err_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_ucast_ctrl_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_pause_err_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_pause_err; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_pause_err_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_pause_err_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_64b_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_64b; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_64b_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_64b_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_65to127b_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_65to127b; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_65to127b_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_65to127b_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_128to255b_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_128to255b; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_128to255b_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_128to255b_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_256to511b_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_256to511b; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_256to511b_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_256to511b_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_512to1023b_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_512to1023b; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_512to1023b_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_512to1023b_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_1024to1518b_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_1024to1518b; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_1024to1518b_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_1024to1518b_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_1519tomaxb_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_1519tomaxb; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_1519tomaxb_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_1519tomaxb_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_oversize_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_oversize; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_oversize_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_oversize_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_mcast_data_ok_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_data_ok; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_mcast_data_ok_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_mcast_data_ok_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_bcast_data_ok_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_data_ok; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_bcast_data_ok_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_bcast_data_ok_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_ucast_data_ok_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_data_ok; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_ucast_data_ok_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_ucast_data_ok_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_mcast_ctrl_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_ctrl; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_mcast_ctrl_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_mcast_ctrl_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_bcast_ctrl_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_ctrl; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_bcast_ctrl_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_bcast_ctrl_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_ucast_ctrl_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_ctrl; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_ucast_ctrl_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_ucast_ctrl_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_pause_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_pause; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_pause_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_pause_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_runt_s
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_runt; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _cntr_rx_runt_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _cntr_rx_runt_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_config_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> rx_cnt_clr; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> rx_parity_err_clr; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> rx_shadow_req; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _cntr_rx_config_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _cntr_rx_config_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _cntr_rx_status_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> rx_parity_err; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> rx_cnt_pause; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _cntr_rx_status_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _cntr_rx_status_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxpayloadoctetsok_s
        {
            reg_t<std::uint64_t,is_reg,63,0> rxpayloadoctetsok; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rxpayloadoctetsok_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _rxpayloadoctetsok_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rxframeoctetsok_s
        {
            reg_t<std::uint64_t,is_reg,63,0> rxframeoctetsok; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rxframeoctetsok_u
        {
            reg_t<std::uint64_t,is_reg> reg;
            _rxframeoctetsok_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_rx_statistics_regs
        {
            _cntr_rx_fragments_u<is_reg> cntr_rx_fragments;
            _cntr_rx_jabbers_u<is_reg> cntr_rx_jabbers;
            _cntr_rx_fcs_u<is_reg> cntr_rx_fcs;
            _cntr_rx_crcerr_u<is_reg> cntr_rx_crcerr;
            _cntr_rx_mcast_data_err_u<is_reg> cntr_rx_mcast_data_err;
            _cntr_rx_bcast_data_err_u<is_reg> cntr_rx_bcast_data_err;
            _cntr_rx_ucast_data_err_u<is_reg> cntr_rx_ucast_data_err;
            _cntr_rx_mcast_ctrl_err_u<is_reg> cntr_rx_mcast_ctrl_err;
            _cntr_rx_bcast_ctrl_err_u<is_reg> cntr_rx_bcast_ctrl_err;
            _cntr_rx_ucast_ctrl_err_u<is_reg> cntr_rx_ucast_ctrl_err;
            _cntr_rx_pause_err_u<is_reg> cntr_rx_pause_err;
            _cntr_rx_64b_u<is_reg> cntr_rx_64b;
            _cntr_rx_65to127b_u<is_reg> cntr_rx_65to127b;
            _cntr_rx_128to255b_u<is_reg> cntr_rx_128to255b;
            _cntr_rx_256to511b_u<is_reg> cntr_rx_256to511b;
            _cntr_rx_512to1023b_u<is_reg> cntr_rx_512to1023b;
            _cntr_rx_1024to1518b_u<is_reg> cntr_rx_1024to1518b;
            _cntr_rx_1519tomaxb_u<is_reg> cntr_rx_1519tomaxb;
            _cntr_rx_oversize_u<is_reg> cntr_rx_oversize;
            _cntr_rx_mcast_data_ok_u<is_reg> cntr_rx_mcast_data_ok;
            _cntr_rx_bcast_data_ok_u<is_reg> cntr_rx_bcast_data_ok;
            _cntr_rx_ucast_data_ok_u<is_reg> cntr_rx_ucast_data_ok;
            _cntr_rx_mcast_ctrl_u<is_reg> cntr_rx_mcast_ctrl;
            _cntr_rx_bcast_ctrl_u<is_reg> cntr_rx_bcast_ctrl;
            _cntr_rx_ucast_ctrl_u<is_reg> cntr_rx_ucast_ctrl;
            _cntr_rx_pause_u<is_reg> cntr_rx_pause;
            _cntr_rx_runt_u<is_reg> cntr_rx_runt;
            reg_t<std::uint8_t,is_reg> reserved1[60];
            _cntr_rx_config_u<is_reg> cntr_rx_config;
            _cntr_rx_status_u<is_reg> cntr_rx_status;
            reg_t<std::uint8_t,is_reg> reserved3[100];
            _rxpayloadoctetsok_u<is_reg> rxpayloadoctetsok;
            _rxframeoctetsok_u<is_reg> rxframeoctetsok;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_rx_statistics_fields
        {
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_fragments; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_jabbers; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_fcs; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_crcerr; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_data_err; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_data_err; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_data_err; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_ctrl_err; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_ctrl_err; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_ctrl_err; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_pause_err; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_64b; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_65to127b; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_128to255b; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_256to511b; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_512to1023b; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_1024to1518b; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_1519tomaxb; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_oversize; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_data_ok; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_data_ok; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_data_ok; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_mcast_ctrl; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_bcast_ctrl; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_ucast_ctrl; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_pause; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> cntr_rx_runt; /*!< [ro] */
            reg_t<std::uint8_t,is_reg> reserved5[60];
            union {
                reg_t<std::uint32_t,is_reg,0,0> rx_cnt_clr; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> rx_parity_err_clr; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> rx_shadow_req; /*!< [rw] */
            };
            union {
                reg_t<std::uint32_t,is_reg,0,0> rx_parity_err; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> rx_cnt_pause; /*!< [rw] */
            };
            reg_t<std::uint8_t,is_reg> reserved7[100];
            reg_t<std::uint64_t,is_reg,63,0> rxpayloadoctetsok; /*!< [ro] */
            reg_t<std::uint64_t,is_reg,63,0> rxframeoctetsok; /*!< [ro] */
        };

        template<bool is_reg=false>
        union Ethernet_100G_rx_statistics_u
        {
            Ethernet_100G_rx_statistics_regs<is_reg> reg;
            Ethernet_100G_rx_statistics_fields<is_reg> fld;
        };

        typedef Ethernet_100G_rx_statistics_fields<true>  Ethernet_100G_rx_statistics_reg_t;
        typedef Ethernet_100G_rx_statistics_fields<false> Ethernet_100G_rx_statistics_mem_t;




        static_assert(8 == sizeof(_cntr_rx_fragments_s<>), "struct _cntr_rx_fragments_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_fragments_u<>), "union _cntr_rx_fragments_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_jabbers_s<>), "struct _cntr_rx_jabbers_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_jabbers_u<>), "union _cntr_rx_jabbers_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_fcs_s<>), "struct _cntr_rx_fcs_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_fcs_u<>), "union _cntr_rx_fcs_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_crcerr_s<>), "struct _cntr_rx_crcerr_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_crcerr_u<>), "union _cntr_rx_crcerr_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_data_err_s<>), "struct _cntr_rx_mcast_data_err_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_data_err_u<>), "union _cntr_rx_mcast_data_err_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_data_err_s<>), "struct _cntr_rx_bcast_data_err_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_data_err_u<>), "union _cntr_rx_bcast_data_err_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_data_err_s<>), "struct _cntr_rx_ucast_data_err_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_data_err_u<>), "union _cntr_rx_ucast_data_err_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_ctrl_err_s<>), "struct _cntr_rx_mcast_ctrl_err_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_ctrl_err_u<>), "union _cntr_rx_mcast_ctrl_err_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_ctrl_err_s<>), "struct _cntr_rx_bcast_ctrl_err_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_ctrl_err_u<>), "union _cntr_rx_bcast_ctrl_err_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_ctrl_err_s<>), "struct _cntr_rx_ucast_ctrl_err_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_ctrl_err_u<>), "union _cntr_rx_ucast_ctrl_err_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_pause_err_s<>), "struct _cntr_rx_pause_err_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_pause_err_u<>), "union _cntr_rx_pause_err_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_64b_s<>), "struct _cntr_rx_64b_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_64b_u<>), "union _cntr_rx_64b_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_65to127b_s<>), "struct _cntr_rx_65to127b_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_65to127b_u<>), "union _cntr_rx_65to127b_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_128to255b_s<>), "struct _cntr_rx_128to255b_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_128to255b_u<>), "union _cntr_rx_128to255b_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_256to511b_s<>), "struct _cntr_rx_256to511b_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_256to511b_u<>), "union _cntr_rx_256to511b_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_512to1023b_s<>), "struct _cntr_rx_512to1023b_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_512to1023b_u<>), "union _cntr_rx_512to1023b_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_1024to1518b_s<>), "struct _cntr_rx_1024to1518b_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_1024to1518b_u<>), "union _cntr_rx_1024to1518b_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_1519tomaxb_s<>), "struct _cntr_rx_1519tomaxb_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_1519tomaxb_u<>), "union _cntr_rx_1519tomaxb_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_oversize_s<>), "struct _cntr_rx_oversize_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_oversize_u<>), "union _cntr_rx_oversize_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_data_ok_s<>), "struct _cntr_rx_mcast_data_ok_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_data_ok_u<>), "union _cntr_rx_mcast_data_ok_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_data_ok_s<>), "struct _cntr_rx_bcast_data_ok_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_data_ok_u<>), "union _cntr_rx_bcast_data_ok_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_data_ok_s<>), "struct _cntr_rx_ucast_data_ok_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_data_ok_u<>), "union _cntr_rx_ucast_data_ok_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_ctrl_s<>), "struct _cntr_rx_mcast_ctrl_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_mcast_ctrl_u<>), "union _cntr_rx_mcast_ctrl_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_ctrl_s<>), "struct _cntr_rx_bcast_ctrl_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_bcast_ctrl_u<>), "union _cntr_rx_bcast_ctrl_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_ctrl_s<>), "struct _cntr_rx_ucast_ctrl_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_ucast_ctrl_u<>), "union _cntr_rx_ucast_ctrl_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_pause_s<>), "struct _cntr_rx_pause_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_pause_u<>), "union _cntr_rx_pause_u does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_runt_s<>), "struct _cntr_rx_runt_s does not have the correct size.");
        static_assert(8 == sizeof(_cntr_rx_runt_u<>), "union _cntr_rx_runt_u does not have the correct size.");
        static_assert(4 == sizeof(_cntr_rx_config_s<>), "struct _cntr_rx_config_s does not have the correct size.");
        static_assert(4 == sizeof(_cntr_rx_config_u<>), "union _cntr_rx_config_u does not have the correct size.");
        static_assert(4 == sizeof(_cntr_rx_status_s<>), "struct _cntr_rx_status_s does not have the correct size.");
        static_assert(4 == sizeof(_cntr_rx_status_u<>), "union _cntr_rx_status_u does not have the correct size.");
        static_assert(8 == sizeof(_rxpayloadoctetsok_s<>), "struct _rxpayloadoctetsok_s does not have the correct size.");
        static_assert(8 == sizeof(_rxpayloadoctetsok_u<>), "union _rxpayloadoctetsok_u does not have the correct size.");
        static_assert(8 == sizeof(_rxframeoctetsok_s<>), "struct _rxframeoctetsok_s does not have the correct size.");
        static_assert(8 == sizeof(_rxframeoctetsok_u<>), "union _rxframeoctetsok_u does not have the correct size.");
        static_assert(400 == sizeof(Ethernet_100G_rx_statistics_regs<>), "struct Ethernet_100G_rx_statistics_regs does not have the correct size.");
        static_assert(400 == sizeof(Ethernet_100G_rx_statistics_fields<>), "struct Ethernet_100G_rx_statistics_fields does not have the correct size.");
        static_assert(400 == sizeof(Ethernet_100G_rx_statistics_u<>), "union Ethernet_100G_rx_statistics_u does not have the correct size.");
        static_assert(400 == sizeof(Ethernet_100G_rx_statistics_reg_t), "type Ethernet_100G_rx_statistics_reg_t does not have the correct size.");
        static_assert(400 == sizeof(Ethernet_100G_rx_statistics_mem_t), "type Ethernet_100G_rx_statistics_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
