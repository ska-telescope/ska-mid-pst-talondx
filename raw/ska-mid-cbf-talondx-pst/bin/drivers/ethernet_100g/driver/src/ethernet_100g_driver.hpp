#ifndef ETH_100G_API_H
#define ETH_100G_API_H

#include <cstdint>
#include <stdexcept>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <thread>
#include <chrono>
#include <bitset>

#include "fpga_driver_base.hpp"
#include "register_map.hpp"
#include "Ethernet_100G_PHY_regset.hpp"
#include "Ethernet_100G_rx_MAC_regset.hpp"
#include "Ethernet_100G_rx_RSFEC_regset.hpp"
#include "Ethernet_100G_rx_statistics_regset.hpp"
#include "Ethernet_100G_tx_MAC_regset.hpp"
#include "Ethernet_100G_tx_RSFEC_regset.hpp"
#include "Ethernet_100G_tx_statistics_regset.hpp"
#include "qsfp_ctrl_regset.hpp"

namespace fpga_driver {
    struct ethernet_100g_param_t {
        uint32_t num_fibres = 4;
        uint32_t num_lanes = 20;
    };

    struct ethernet_100g_config_t {
        bool rx_loopback_enable = false;
    };

    struct ethernet_100g_status_t {
        struct phy_t {
            bool rx_loopback;
            std::vector<bool> rx_freq_lock;
            std::vector<bool> rx_word_lock;

            phy_t() : rx_freq_lock(4), rx_word_lock(20)  {}

            // Constructor to initialize the number of channels and polarisations actually set in the parameters
            phy_t(uint32_t num_fibres, uint32_t num_lanes)
                : rx_freq_lock(num_fibres), rx_word_lock(num_lanes) {}
        } phy;

        struct fec_t {
            uint32_t rx_corrected_code_words;
            uint32_t rx_uncorrected_code_words;
        } fec;

        struct mac_t {
            uint32_t rx_fragments;
            uint32_t rx_runt;
            uint32_t rx_64_bytes;
            uint32_t rx_65_to_127_bytes;
            uint32_t rx_128_to_255_bytes;
            uint32_t rx_256_to_511_bytes;
            uint32_t rx_512_to_1023_bytes;
            uint32_t rx_1024_to_1518_bytes;
            uint32_t rx_1519_to_max_bytes;
            uint32_t rx_oversize;
            uint32_t rx_frame_octets_ok;
            uint32_t rx_crcerr;
            uint32_t tx_fragments;
            uint32_t tx_runt;
            uint32_t tx_64_bytes;
            uint32_t tx_65_to_127_bytes;
            uint32_t tx_128_to_255_bytes;
            uint32_t tx_256_to_511_bytes;
            uint32_t tx_512_to_1023_bytes;
            uint32_t tx_1024_to_1518_bytes;
            uint32_t tx_1519_to_max_bytes;
            uint32_t tx_oversize;
            uint32_t tx_frame_octets_ok;
            uint32_t tx_crcerr;
        } mac;

        ethernet_100g_status_t(uint32_t num_fibres = 4, uint32_t num_lanes = 20) : phy(num_fibres, num_lanes) {}

    };

    class Ethernet_100g_Driver : public fpga_driver::Base<ethernet_100g_config_t, ethernet_100g_status_t>
    {
        public:
            Ethernet_100g_Driver(ethernet_100g_param_t &params, RegisterSetInfos &regset, Logger &logger)
            : fpga_driver::Base<ethernet_100g_config_t,ethernet_100g_status_t>(regset, logger), params_(params) {
                auto *phy_reg_map = new RegisterMap<Ethernet_100G_PHY_regset::Ethernet_100G_PHY_reg_t, Ethernet_100G_PHY_regset::Ethernet_100G_PHY_mem_t>(get_regset_info("phy", Ethernet_100G_PHY_regset::version));
                auto *rx_mac_reg_map = new RegisterMap<Ethernet_100G_rx_MAC_regset::Ethernet_100G_rx_MAC_reg_t, Ethernet_100G_rx_MAC_regset::Ethernet_100G_rx_MAC_mem_t>(get_regset_info("rx_mac", Ethernet_100G_rx_MAC_regset::version));
                auto *rx_rsfec_reg_map = new RegisterMap<Ethernet_100G_rx_RSFEC_regset::Ethernet_100G_rx_RSFEC_reg_t, Ethernet_100G_rx_RSFEC_regset::Ethernet_100G_rx_RSFEC_mem_t>(get_regset_info("rx_rsfec", Ethernet_100G_rx_RSFEC_regset::version));
                auto *rx_stats_reg_map = new RegisterMap<Ethernet_100G_rx_statistics_regset::Ethernet_100G_rx_statistics_reg_t, Ethernet_100G_rx_statistics_regset::Ethernet_100G_rx_statistics_mem_t>(get_regset_info("rx_statistics", Ethernet_100G_rx_statistics_regset::version));
                auto *tx_mac_reg_map = new RegisterMap<Ethernet_100G_tx_MAC_regset::Ethernet_100G_tx_MAC_reg_t, Ethernet_100G_tx_MAC_regset::Ethernet_100G_tx_MAC_mem_t>(get_regset_info("tx_mac", Ethernet_100G_tx_MAC_regset::version));
                auto *tx_rsfec_reg_map = new RegisterMap<Ethernet_100G_tx_RSFEC_regset::Ethernet_100G_tx_RSFEC_reg_t, Ethernet_100G_tx_RSFEC_regset::Ethernet_100G_tx_RSFEC_mem_t>(get_regset_info("tx_rsfec", Ethernet_100G_tx_RSFEC_regset::version));
                auto *tx_stats_reg_map = new RegisterMap<Ethernet_100G_tx_statistics_regset::Ethernet_100G_tx_statistics_reg_t, Ethernet_100G_tx_statistics_regset::Ethernet_100G_tx_statistics_mem_t>(get_regset_info("rx_statistics", Ethernet_100G_tx_statistics_regset::version));
                phy_ = phy_reg_map->regs();
                rx_mac_ = rx_mac_reg_map->regs();
                rx_rsfec_ = rx_rsfec_reg_map->regs();
                rx_stats_ = rx_stats_reg_map->regs();
                tx_mac_ = tx_mac_reg_map->regs();
                tx_rsfec_ = tx_rsfec_reg_map->regs();
                tx_stats_ = tx_stats_reg_map->regs();
            };

            ~Ethernet_100g_Driver() {
                // Free the memory that was created in the constructor.
                phy_.reset();
                rx_mac_.reset();
                rx_rsfec_.reset();
                rx_stats_.reset();
                tx_mac_.reset();
                tx_rsfec_.reset();
                tx_stats_.reset();
            };

            virtual void get_default_config(ethernet_100g_config_t &config) {
                config.rx_loopback_enable = false;
            };


    private:
        std::shared_ptr<Ethernet_100G_PHY_regset::Ethernet_100G_PHY_reg_t> phy_ = nullptr;
        std::shared_ptr<Ethernet_100G_rx_MAC_regset::Ethernet_100G_rx_MAC_reg_t> rx_mac_ = nullptr;
        std::shared_ptr<Ethernet_100G_rx_RSFEC_regset::Ethernet_100G_rx_RSFEC_reg_t> rx_rsfec_ = nullptr;
        std::shared_ptr<Ethernet_100G_rx_statistics_regset::Ethernet_100G_rx_statistics_reg_t> rx_stats_ = nullptr;
        std::shared_ptr<Ethernet_100G_tx_MAC_regset::Ethernet_100G_tx_MAC_reg_t> tx_mac_ = nullptr;
        std::shared_ptr<Ethernet_100G_tx_RSFEC_regset::Ethernet_100G_tx_RSFEC_reg_t> tx_rsfec_ = nullptr;
        std::shared_ptr<Ethernet_100G_tx_statistics_regset::Ethernet_100G_tx_statistics_reg_t> tx_stats_ = nullptr;
        
        ethernet_100g_param_t params_;

        // Keep an internal accumulator for the corrected and uncorrected codewords, since these registers are cleared when read.
        uint32_t rx_corrected_code_words_;
        uint32_t rx_uncorrected_code_words_;

        virtual void recover_() {
            // Enable reset
            phy_->eio_sys_rst = true;
            phy_->soft_rxp_rst = true;

            rx_stats_->rx_cnt_clr = 1;
            rx_stats_->rx_cnt_clr = 0;
            tx_stats_->tx_cnt_clr = 1;
            tx_stats_->tx_cnt_clr = 0;

        };

        virtual void configure_(const ethernet_100g_config_t &config) {
            log->info("Resetting...");
            phy_->eio_sys_rst = true;
            uint32_t num_lanes = params_.num_lanes;
            uint32_t num_fibres = params_.num_fibres;
            uint32_t all_lanes = (UINT32_C(1) << num_lanes) - 1;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));

            // Apply loopback:
            log->info("Ethernet PHY setting loopback mode = " + std::to_string(config.rx_loopback_enable));
            phy_->eio_sloop = static_cast<int>(config.rx_loopback_enable) * 0xF;
            phy_->eio_sys_rst = false;

            // Check for word lock on all lanes:
            log->info("Ethernet PHY waiting to achieve word lock on all " + std::to_string(num_lanes) + " lanes.");
            uint32_t lock = 0;
            for (int wait = 0; wait < 50; wait++) {
                uint32_t new_lock = phy_->word_lock;
                if (new_lock != lock) {
                    log->info("Ethernet PHY word lock = " + std::bitset<64>(new_lock).to_string().substr(64 - num_lanes, num_lanes));
                }
                lock = new_lock;
                if (lock == all_lanes) {
                    log->info("Ethernet PHY achieved word lock on all " + std::to_string(num_lanes) + " lanes.");
                    break;
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
            
            if (lock != all_lanes) {
                uint32_t freq_lock = phy_->eio_freq_lock;
                log->info("RX frequency lock: " + std::bitset<64>(freq_lock).to_string().substr(64 - num_fibres, num_lanes));
                log->error("Ethernet PHY failed to achieve word lock on all " + std::to_string(num_lanes) + " lanes. Got locked lanes " + std::bitset<20>(lock).to_string());
            }


        };

        virtual void deconfigure_(ethernet_100g_config_t config) {
            // Remove loopback? Just set eio_sloop to false?
            // phy_->eio_sloop = config.rx_loopback_enable;
            phy_->eio_sloop = false;

        };

        virtual void status_(ethernet_100g_status_t &status, bool clear) {
            // Populate the status struct, optionally clear the counters
            log->info("Getting status of ethernet_100g...");

            // Read RX counters
            rx_stats_->rx_shadow_req = 1;

            status.mac.rx_fragments = rx_stats_->cntr_rx_fragments;
            status.mac.rx_runt = rx_stats_->cntr_rx_runt;
            status.mac.rx_64_bytes = rx_stats_->cntr_rx_64b;
            status.mac.rx_65_to_127_bytes = rx_stats_->cntr_rx_65to127b;
            status.mac.rx_65_to_127_bytes = rx_stats_->cntr_rx_65to127b;
            status.mac.rx_128_to_255_bytes = rx_stats_->cntr_rx_128to255b;
            status.mac.rx_256_to_511_bytes = rx_stats_->cntr_rx_256to511b;
            status.mac.rx_512_to_1023_bytes = rx_stats_->cntr_rx_512to1023b;
            status.mac.rx_1024_to_1518_bytes = rx_stats_->cntr_rx_1024to1518b;
            status.mac.rx_1519_to_max_bytes = rx_stats_->cntr_rx_1519tomaxb;
            status.mac.rx_oversize = rx_stats_->cntr_rx_oversize;
            status.mac.rx_frame_octets_ok = rx_stats_->rxframeoctetsok;
            status.mac.rx_crcerr = rx_stats_->cntr_rx_crcerr;

            rx_stats_->rx_shadow_req = 0;

            // Read TX counters
            tx_stats_->tx_shadow_req = 1;

            status.mac.tx_fragments = tx_stats_->cntr_tx_fragments;
            status.mac.tx_runt = tx_stats_->cntr_tx_runt;
            status.mac.tx_64_bytes = tx_stats_->cntr_tx_64b;
            status.mac.tx_65_to_127_bytes = tx_stats_->cntr_tx_65to127b;
            status.mac.tx_65_to_127_bytes = tx_stats_->cntr_tx_65to127b;
            status.mac.tx_128_to_255_bytes = tx_stats_->cntr_tx_128to255b;
            status.mac.tx_256_to_511_bytes = tx_stats_->cntr_tx_256to511b;
            status.mac.tx_512_to_1023_bytes = tx_stats_->cntr_tx_512to1023b;
            status.mac.tx_1024_to_1518_bytes = tx_stats_->cntr_tx_1024to1518b;
            status.mac.tx_1519_to_max_bytes = tx_stats_->cntr_tx_1519tomaxb;
            status.mac.tx_oversize = tx_stats_->cntr_tx_oversize;
            status.mac.tx_frame_octets_ok = tx_stats_->txframeoctetsok;
            status.mac.tx_crcerr = tx_stats_->cntr_tx_crcerr;

            tx_stats_->tx_shadow_req = 0;

            // Optionally clear the counters. Codeword registers are cleared when read, so clear internal counters for these.
            if (clear == true) {
                rx_stats_->rx_cnt_clr = 1;
                rx_stats_->rx_cnt_clr = 0;
                tx_stats_->tx_cnt_clr = 1;
                tx_stats_->tx_cnt_clr = 0;
                rx_corrected_code_words_ = 0;
                rx_uncorrected_code_words_ = 0;
            }

            // Read codeword registers and add the read values to the driver's internal counters
            rx_corrected_code_words_ += rx_rsfec_->corrected_cw;
            rx_uncorrected_code_words_ += rx_rsfec_->uncorrected_cw;

            // The status struct is returned with this internal CWs count, rather than the reg read values.
            status.fec.rx_corrected_code_words = rx_corrected_code_words_;
            status.fec.rx_uncorrected_code_words = rx_uncorrected_code_words_;

            log->info("Corrected codewords: " + std::to_string(status.fec.rx_corrected_code_words));
            log->info("Uncorrected codewords: " + std::to_string(status.fec.rx_uncorrected_code_words));

            // Read the PHY registers
            uint32_t sloop_rval = phy_->eio_sloop;
            if (sloop_rval == 0xF) {
                status.phy.rx_loopback = true;
                log->info("RX loopback is enabled.");
            } else {
                status.phy.rx_loopback = false;
                log->info("RX loopback is disabled.");
            }

            uint32_t word_lock_rval = phy_->word_lock;
            uint32_t freq_lock_rval = phy_->eio_freq_lock;
            for (uint32_t i = 0; i < status.phy.rx_word_lock.size(); i++) {
                status.phy.rx_word_lock[i] = (word_lock_rval >> i) & 1;
            }
            for (uint32_t i = 0; i < status.phy.rx_freq_lock.size(); i++) {
                status.phy.rx_freq_lock[i] = (freq_lock_rval >> i) & 1;
            }
        };

        virtual void start_() {
            log->info("Starting ethernet_100g...");
        };

        virtual void stop_(bool force = false) {
            log->info("Stopping ethernet_100g...");
        };

    };

}

#endif // ETH_100G_API_H