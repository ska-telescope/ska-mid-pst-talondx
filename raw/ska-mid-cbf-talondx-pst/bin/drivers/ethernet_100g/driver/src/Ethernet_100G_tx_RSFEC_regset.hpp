
#ifndef __ETHERNET_100G_TX_RSFEC_REGSET__H__
    #define __ETHERNET_100G_TX_RSFEC_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace Ethernet_100G_tx_RSFEC_regset {

        const std::string mnemonic = "Ethernet_100G_tx_RSFEC";
        const std::string version = "19.2.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _tx_rsfec_revid_s
        {
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_revid; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _tx_rsfec_revid_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _tx_rsfec_revid_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _tx_rsfec_scratch_s
        {
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_scratch; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _tx_rsfec_scratch_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _tx_rsfec_scratch_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _tx_rsfec_name_0_s
        {
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_name_0; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _tx_rsfec_name_0_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _tx_rsfec_name_0_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _tx_rsfec_name_1_s
        {
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_name_1; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _tx_rsfec_name_1_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _tx_rsfec_name_1_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _tx_rsfec_name_2_s
        {
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_name_2; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _tx_rsfec_name_2_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _tx_rsfec_name_2_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _err_ins_en_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> err_ins_all; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> err_ins_single; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _err_ins_en_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _err_ins_en_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _err_mask_s
        {
            union {
                reg_t<std::uint32_t,is_reg,3,0> group_num; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,17,8> bit_mask; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,24,24> sym_32; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _err_mask_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _err_mask_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _symbol_err_mask_s
        {
            reg_t<std::uint32_t,is_reg,31,0> symbol_err_mask; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _symbol_err_mask_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _symbol_err_mask_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_tx_RSFEC_regs
        {
            _tx_rsfec_revid_u<is_reg> tx_rsfec_revid;
            _tx_rsfec_scratch_u<is_reg> tx_rsfec_scratch;
            _tx_rsfec_name_0_u<is_reg> tx_rsfec_name_0;
            _tx_rsfec_name_1_u<is_reg> tx_rsfec_name_1;
            _tx_rsfec_name_2_u<is_reg> tx_rsfec_name_2;
            _err_ins_en_u<is_reg> err_ins_en;
            _err_mask_u<is_reg> err_mask;
            _symbol_err_mask_u<is_reg> symbol_err_mask;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_tx_RSFEC_fields
        {
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_revid; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_scratch; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_name_0; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_name_1; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> tx_rsfec_name_2; /*!< [ro] */
            union {
                reg_t<std::uint32_t,is_reg,0,0> err_ins_all; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> err_ins_single; /*!< [rw] */
            };
            union {
                reg_t<std::uint32_t,is_reg,3,0> group_num; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,17,8> bit_mask; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,24,24> sym_32; /*!< [rw] */
            };
            reg_t<std::uint32_t,is_reg,31,0> symbol_err_mask; /*!< [rw] */
        };

        template<bool is_reg=false>
        union Ethernet_100G_tx_RSFEC_u
        {
            Ethernet_100G_tx_RSFEC_regs<is_reg> reg;
            Ethernet_100G_tx_RSFEC_fields<is_reg> fld;
        };

        typedef Ethernet_100G_tx_RSFEC_fields<true>  Ethernet_100G_tx_RSFEC_reg_t;
        typedef Ethernet_100G_tx_RSFEC_fields<false> Ethernet_100G_tx_RSFEC_mem_t;




        static_assert(4 == sizeof(_tx_rsfec_revid_s<>), "struct _tx_rsfec_revid_s does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_revid_u<>), "union _tx_rsfec_revid_u does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_scratch_s<>), "struct _tx_rsfec_scratch_s does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_scratch_u<>), "union _tx_rsfec_scratch_u does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_name_0_s<>), "struct _tx_rsfec_name_0_s does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_name_0_u<>), "union _tx_rsfec_name_0_u does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_name_1_s<>), "struct _tx_rsfec_name_1_s does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_name_1_u<>), "union _tx_rsfec_name_1_u does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_name_2_s<>), "struct _tx_rsfec_name_2_s does not have the correct size.");
        static_assert(4 == sizeof(_tx_rsfec_name_2_u<>), "union _tx_rsfec_name_2_u does not have the correct size.");
        static_assert(4 == sizeof(_err_ins_en_s<>), "struct _err_ins_en_s does not have the correct size.");
        static_assert(4 == sizeof(_err_ins_en_u<>), "union _err_ins_en_u does not have the correct size.");
        static_assert(4 == sizeof(_err_mask_s<>), "struct _err_mask_s does not have the correct size.");
        static_assert(4 == sizeof(_err_mask_u<>), "union _err_mask_u does not have the correct size.");
        static_assert(4 == sizeof(_symbol_err_mask_s<>), "struct _symbol_err_mask_s does not have the correct size.");
        static_assert(4 == sizeof(_symbol_err_mask_u<>), "union _symbol_err_mask_u does not have the correct size.");
        static_assert(32 == sizeof(Ethernet_100G_tx_RSFEC_regs<>), "struct Ethernet_100G_tx_RSFEC_regs does not have the correct size.");
        static_assert(32 == sizeof(Ethernet_100G_tx_RSFEC_fields<>), "struct Ethernet_100G_tx_RSFEC_fields does not have the correct size.");
        static_assert(32 == sizeof(Ethernet_100G_tx_RSFEC_u<>), "union Ethernet_100G_tx_RSFEC_u does not have the correct size.");
        static_assert(32 == sizeof(Ethernet_100G_tx_RSFEC_reg_t), "type Ethernet_100G_tx_RSFEC_reg_t does not have the correct size.");
        static_assert(32 == sizeof(Ethernet_100G_tx_RSFEC_mem_t), "type Ethernet_100G_tx_RSFEC_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
