
#ifndef __ETHERNET_100G_RX_RSFEC_REGSET__H__
    #define __ETHERNET_100G_RX_RSFEC_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace Ethernet_100G_rx_RSFEC_regset {

        const std::string mnemonic = "Ethernet_100G_rx_RSFEC";
        const std::string version = "19.2.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_rsfec_revid_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_revid; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rx_rsfec_revid_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_rsfec_revid_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_rsfec_scratch_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_scratch; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _rx_rsfec_scratch_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_rsfec_scratch_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_rsfec_name_0_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_name_0; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rx_rsfec_name_0_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_rsfec_name_0_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_rsfec_name_1_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_name_1; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rx_rsfec_name_1_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_rsfec_name_1_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_rsfec_name_2_s
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_name_2; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _rx_rsfec_name_2_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_rsfec_name_2_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _bypass_restart_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> bypass; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> restart; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _bypass_restart_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _bypass_restart_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _rx_fec_status_s
        {
            union {
                reg_t<std::uint32_t,is_reg,3,0> amps_lock; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,4,4> fec_align_status; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,15,8> fec_lane; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union _rx_fec_status_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _rx_fec_status_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _corrected_cw_s
        {
            reg_t<std::uint32_t,is_reg,31,0> corrected_cw; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _corrected_cw_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _corrected_cw_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _uncorrected_cw_s
        {
            reg_t<std::uint32_t,is_reg,31,0> uncorrected_cw; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _uncorrected_cw_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _uncorrected_cw_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_rx_RSFEC_regs
        {
            _rx_rsfec_revid_u<is_reg> rx_rsfec_revid;
            _rx_rsfec_scratch_u<is_reg> rx_rsfec_scratch;
            _rx_rsfec_name_0_u<is_reg> rx_rsfec_name_0;
            _rx_rsfec_name_1_u<is_reg> rx_rsfec_name_1;
            _rx_rsfec_name_2_u<is_reg> rx_rsfec_name_2;
            _bypass_restart_u<is_reg> bypass_restart;
            _rx_fec_status_u<is_reg> rx_fec_status;
            _corrected_cw_u<is_reg> corrected_cw;
            _uncorrected_cw_u<is_reg> uncorrected_cw;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) Ethernet_100G_rx_RSFEC_fields
        {
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_revid; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_scratch; /*!< [rw] */
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_name_0; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_name_1; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> rx_rsfec_name_2; /*!< [ro] */
            union {
                reg_t<std::uint32_t,is_reg,0,0> bypass; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,4,4> restart; /*!< [rw] */
            };
            union {
                reg_t<std::uint32_t,is_reg,3,0> amps_lock; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,4,4> fec_align_status; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,15,8> fec_lane; /*!< [ro] */
            };
            reg_t<std::uint32_t,is_reg,31,0> corrected_cw; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> uncorrected_cw; /*!< [ro] */
        };

        template<bool is_reg=false>
        union Ethernet_100G_rx_RSFEC_u
        {
            Ethernet_100G_rx_RSFEC_regs<is_reg> reg;
            Ethernet_100G_rx_RSFEC_fields<is_reg> fld;
        };

        typedef Ethernet_100G_rx_RSFEC_fields<true>  Ethernet_100G_rx_RSFEC_reg_t;
        typedef Ethernet_100G_rx_RSFEC_fields<false> Ethernet_100G_rx_RSFEC_mem_t;




        static_assert(4 == sizeof(_rx_rsfec_revid_s<>), "struct _rx_rsfec_revid_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_revid_u<>), "union _rx_rsfec_revid_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_scratch_s<>), "struct _rx_rsfec_scratch_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_scratch_u<>), "union _rx_rsfec_scratch_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_name_0_s<>), "struct _rx_rsfec_name_0_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_name_0_u<>), "union _rx_rsfec_name_0_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_name_1_s<>), "struct _rx_rsfec_name_1_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_name_1_u<>), "union _rx_rsfec_name_1_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_name_2_s<>), "struct _rx_rsfec_name_2_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_rsfec_name_2_u<>), "union _rx_rsfec_name_2_u does not have the correct size.");
        static_assert(4 == sizeof(_bypass_restart_s<>), "struct _bypass_restart_s does not have the correct size.");
        static_assert(4 == sizeof(_bypass_restart_u<>), "union _bypass_restart_u does not have the correct size.");
        static_assert(4 == sizeof(_rx_fec_status_s<>), "struct _rx_fec_status_s does not have the correct size.");
        static_assert(4 == sizeof(_rx_fec_status_u<>), "union _rx_fec_status_u does not have the correct size.");
        static_assert(4 == sizeof(_corrected_cw_s<>), "struct _corrected_cw_s does not have the correct size.");
        static_assert(4 == sizeof(_corrected_cw_u<>), "union _corrected_cw_u does not have the correct size.");
        static_assert(4 == sizeof(_uncorrected_cw_s<>), "struct _uncorrected_cw_s does not have the correct size.");
        static_assert(4 == sizeof(_uncorrected_cw_u<>), "union _uncorrected_cw_u does not have the correct size.");
        static_assert(36 == sizeof(Ethernet_100G_rx_RSFEC_regs<>), "struct Ethernet_100G_rx_RSFEC_regs does not have the correct size.");
        static_assert(36 == sizeof(Ethernet_100G_rx_RSFEC_fields<>), "struct Ethernet_100G_rx_RSFEC_fields does not have the correct size.");
        static_assert(36 == sizeof(Ethernet_100G_rx_RSFEC_u<>), "union Ethernet_100G_rx_RSFEC_u does not have the correct size.");
        static_assert(36 == sizeof(Ethernet_100G_rx_RSFEC_reg_t), "type Ethernet_100G_rx_RSFEC_reg_t does not have the correct size.");
        static_assert(36 == sizeof(Ethernet_100G_rx_RSFEC_mem_t), "type Ethernet_100G_rx_RSFEC_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
