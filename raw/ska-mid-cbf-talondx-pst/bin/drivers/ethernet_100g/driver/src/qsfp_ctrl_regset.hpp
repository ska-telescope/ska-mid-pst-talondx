
#ifndef __QSFP_CTRL_REGSET__H__
    #define __QSFP_CTRL_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace qsfp_ctrl_regset {

        const std::string mnemonic = "qsfp_ctrl";
        const std::string version = "1.0.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _ctrl_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> mod_sel_n; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> reset_n; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> lowpwr; /*!< [rw] */
            };
        };

        template<bool is_reg=false>
        union _ctrl_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _ctrl_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _status_s
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> mod_prs_n; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> interrupt_n; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union _status_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _status_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) qsfp_ctrl_regs
        {
            _ctrl_u<is_reg> ctrl;
            _status_u<is_reg> status;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) qsfp_ctrl_fields
        {
            union {
                reg_t<std::uint32_t,is_reg,0,0> mod_sel_n; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,1,1> reset_n; /*!< [rw] */
                reg_t<std::uint32_t,is_reg,2,2> lowpwr; /*!< [rw] */
            };
            union {
                reg_t<std::uint32_t,is_reg,0,0> mod_prs_n; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,1,1> interrupt_n; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union qsfp_ctrl_u
        {
            qsfp_ctrl_regs<is_reg> reg;
            qsfp_ctrl_fields<is_reg> fld;
        };

        typedef qsfp_ctrl_fields<true>  qsfp_ctrl_reg_t;
        typedef qsfp_ctrl_fields<false> qsfp_ctrl_mem_t;




        static_assert(4 == sizeof(_ctrl_s<>), "struct _ctrl_s does not have the correct size.");
        static_assert(4 == sizeof(_ctrl_u<>), "union _ctrl_u does not have the correct size.");
        static_assert(4 == sizeof(_status_s<>), "struct _status_s does not have the correct size.");
        static_assert(4 == sizeof(_status_u<>), "union _status_u does not have the correct size.");
        static_assert(8 == sizeof(qsfp_ctrl_regs<>), "struct qsfp_ctrl_regs does not have the correct size.");
        static_assert(8 == sizeof(qsfp_ctrl_fields<>), "struct qsfp_ctrl_fields does not have the correct size.");
        static_assert(8 == sizeof(qsfp_ctrl_u<>), "union qsfp_ctrl_u does not have the correct size.");
        static_assert(8 == sizeof(qsfp_ctrl_reg_t), "type qsfp_ctrl_reg_t does not have the correct size.");
        static_assert(8 == sizeof(qsfp_ctrl_mem_t), "type qsfp_ctrl_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
