#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "src/ethernet_100g_driver.hpp"

namespace py = pybind11;
using namespace fpga_driver;

FPGA_DRIVER_PYBIND_MODULE_DECLARATION(ethernet_100g, m) {
    py::class_<ethernet_100g_param_t>(m, "param_t")
        .def(py::init<>())
        .def_readwrite("num_fibres", &ethernet_100g_param_t::num_fibres)
        .def_readwrite("num_lanes", &ethernet_100g_param_t::num_lanes);

    py::class_<ethernet_100g_config_t>(m, "config_t")
        .def(py::init<>())
        .def(py::init<bool>(),
             py::arg("rx_loopback_enable") = false)
        .def_readwrite("rx_loopback_enable", &ethernet_100g_config_t::rx_loopback_enable);

    py::class_<ethernet_100g_status_t>(m, "status_t")
        .def(py::init<>())
        .def(py::init<uint32_t, uint32_t>())
        .def_readwrite("phy", &ethernet_100g_status_t::phy)
        .def_readwrite("fec", &ethernet_100g_status_t::fec)
        .def_readwrite("mac", &ethernet_100g_status_t::mac);

    py::class_<ethernet_100g_status_t::phy_t>(m, "phy_t")
        .def(py::init<>())
        .def_readwrite("rx_loopback", &ethernet_100g_status_t::phy_t::rx_loopback)
        .def_readwrite("rx_freq_lock", &ethernet_100g_status_t::phy_t::rx_freq_lock)
        .def_readwrite("rx_word_lock", &ethernet_100g_status_t::phy_t::rx_word_lock);

    py::class_<ethernet_100g_status_t::fec_t>(m, "fec_t")
        .def(py::init<>())
        .def_readwrite("rx_corrected_code_words", &ethernet_100g_status_t::fec_t::rx_corrected_code_words)
        .def_readwrite("rx_uncorrected_code_words", &ethernet_100g_status_t::fec_t::rx_uncorrected_code_words);


    py::class_<ethernet_100g_status_t::mac_t>(m, "mac_t")
        .def(py::init<>())
        .def_readwrite("rx_fragments", &ethernet_100g_status_t::mac_t::rx_fragments)
        .def_readwrite("rx_runt", &ethernet_100g_status_t::mac_t::rx_runt)
        .def_readwrite("rx_64_bytes", &ethernet_100g_status_t::mac_t::rx_64_bytes)
        .def_readwrite("rx_65_to_127_bytes", &ethernet_100g_status_t::mac_t::rx_65_to_127_bytes)
        .def_readwrite("rx_128_to_255_bytes", &ethernet_100g_status_t::mac_t::rx_128_to_255_bytes)
        .def_readwrite("rx_256_to_511_bytes", &ethernet_100g_status_t::mac_t::rx_256_to_511_bytes)
        .def_readwrite("rx_512_to_1023_bytes", &ethernet_100g_status_t::mac_t::rx_512_to_1023_bytes)
        .def_readwrite("rx_1024_to_1518_bytes", &ethernet_100g_status_t::mac_t::rx_1024_to_1518_bytes)
        .def_readwrite("rx_1519_to_max_bytes", &ethernet_100g_status_t::mac_t::rx_1519_to_max_bytes)
        .def_readwrite("rx_oversize", &ethernet_100g_status_t::mac_t::rx_oversize)
        .def_readwrite("rx_frame_octets_ok", &ethernet_100g_status_t::mac_t::rx_frame_octets_ok)
        .def_readwrite("rx_crcerr", &ethernet_100g_status_t::mac_t::rx_crcerr)
        .def_readwrite("tx_fragments", &ethernet_100g_status_t::mac_t::tx_fragments)
        .def_readwrite("tx_runt", &ethernet_100g_status_t::mac_t::tx_runt)
        .def_readwrite("tx_64_bytes", &ethernet_100g_status_t::mac_t::tx_64_bytes)
        .def_readwrite("tx_65_to_127_bytes", &ethernet_100g_status_t::mac_t::tx_65_to_127_bytes)
        .def_readwrite("tx_128_to_255_bytes", &ethernet_100g_status_t::mac_t::tx_128_to_255_bytes)
        .def_readwrite("tx_256_to_511_bytes", &ethernet_100g_status_t::mac_t::tx_256_to_511_bytes)
        .def_readwrite("tx_512_to_1023_bytes", &ethernet_100g_status_t::mac_t::tx_512_to_1023_bytes)
        .def_readwrite("tx_1024_to_1518_bytes", &ethernet_100g_status_t::mac_t::tx_1024_to_1518_bytes)
        .def_readwrite("tx_1519_to_max_bytes", &ethernet_100g_status_t::mac_t::tx_1519_to_max_bytes)
        .def_readwrite("tx_oversize", &ethernet_100g_status_t::mac_t::tx_oversize)
        .def_readwrite("tx_frame_octets_ok", &ethernet_100g_status_t::mac_t::tx_frame_octets_ok)
        .def_readwrite("tx_crcerr", &ethernet_100g_status_t::mac_t::tx_crcerr);


    py::class_<Ethernet_100g_Driver>(m, "driver")
        .def(py::init<ethernet_100g_param_t&, RegisterSetInfos&, Logger&>())
        .def("recover", &Ethernet_100g_Driver::recover)
        .def("configure", &Ethernet_100g_Driver::configure)
        .def("deconfigure", &Ethernet_100g_Driver::deconfigure)
        .def("start", &Ethernet_100g_Driver::start)
        .def("stop", &Ethernet_100g_Driver::stop)
        .def("status", &Ethernet_100g_Driver::status)
        .def("get_default_config", &Ethernet_100g_Driver::get_default_config);
}