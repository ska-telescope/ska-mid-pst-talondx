files = [
    "Ethernet_100G_PHY.json",
    "Ethernet_100G_rx_MAC.json",
    "Ethernet_100G_rx_RSFEC.json",
    "Ethernet_100G_rx_statistics.json",
    "Ethernet_100G_tx_MAC.json",
    "Ethernet_100G_tx_RSFEC.json",
    "Ethernet_100G_tx_statistics.json",
    "qsfp_ctrl.json",
]
