#include <pybind11/pybind11.h>
namespace py = pybind11;

void init_ethernet_100g_submodule(py::module &);
void init_fpga_driver_base_submodule(py::module &);
void init_sys_id_submodule(py::module &);
void init_vcc_ch20_submodule(py::module &);
void init_circuit_switch_submodule(py::module &);
void init_wideband_input_buffer_submodule(py::module &);
void init_frequency_shifter_submodule(py::module &);

PYBIND11_MODULE(talon_dx_tdc_base_tdc_vcc_pst_processing, m) {
   py::module ethernet_100g_module = m.def_submodule("ethernet_100g", "ethernet_100g module");
   init_ethernet_100g_submodule(ethernet_100g_module);

   py::module fpga_driver_base_module = m.def_submodule("fpga_driver_base", "fpga_driver_base module");
   init_fpga_driver_base_submodule(fpga_driver_base_module);

   py::module sys_id_module = m.def_submodule("sys_id", "sys_id module");
   init_sys_id_submodule(sys_id_module);

   py::module vcc_ch20_module = m.def_submodule("vcc_ch20", "vcc_ch20 module");
   init_vcc_ch20_submodule(vcc_ch20_module);

   py::module circuit_switch_module = m.def_submodule("circuit_switch", "circuit_switch module");
   init_circuit_switch_submodule(circuit_switch_module);

   py::module wideband_input_buffer_module = m.def_submodule("wideband_input_buffer", "wideband_input_buffer module");
   init_wideband_input_buffer_submodule(wideband_input_buffer_module);

   py::module frequency_shifter_module = m.def_submodule("frequency_shifter", "frequency_shifter module");
   init_frequency_shifter_submodule(frequency_shifter_module);

}
