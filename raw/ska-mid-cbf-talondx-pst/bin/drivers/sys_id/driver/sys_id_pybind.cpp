#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/iostream.h>
#include <pybind11/operators.h>
#include <pybind11/functional.h>

#include <src/sys_id_driver.hpp>

namespace py = pybind11;
using namespace fpga_driver;

FPGA_DRIVER_PYBIND_MODULE_DECLARATION(sys_id, m) {
    py::class_<sys_id_param_t>(m, "csr_param_t")
        .def(py::init<>());

    py::class_<sys_id_status_t>(m, "status_t")
        .def(py::init<>())
        .def_readwrite("bitstream", &sys_id_status_t::bitstream)
        .def_readwrite("major", &sys_id_status_t::major)
        .def_readwrite("minor", &sys_id_status_t::minor)
        .def_readwrite("patch", &sys_id_status_t::patch)
        .def_readwrite("prerelease", &sys_id_status_t::prerelease)
        .def_readwrite("commit", &sys_id_status_t::commit)
        .def_readwrite("scratch", &sys_id_status_t::scratch)
        .def_readwrite("version", &sys_id_status_t::version);

    py::class_<sys_id_config_t>(m, "config_t")
        .def(py::init<>())
        .def_readwrite("scratch", &sys_id_config_t::scratch);

    py::class_<fpga_driver::Sys_ID_Driver>(m, "driver")
        .def(py::init<RegisterSetInfos&, Logger&>())
        .def("configure", &fpga_driver::Sys_ID_Driver::configure)
        .def("status", &fpga_driver::Sys_ID_Driver::status);
}

