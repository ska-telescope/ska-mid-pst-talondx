#ifndef __SYS_ID_DRIVER_H
#define __SYS_ID_DRIVER_H

#include "sys_id_regset.hpp"
#include "fpga_driver_base.hpp"
#include <any>

namespace fpga_driver {

    struct sys_id_param_t {
    };

    struct sys_id_config_t {
        uint32_t scratch;
    };

    struct sys_id_status_t {
        uint32_t bitstream;
        uint8_t  major;
        uint8_t  minor;
        uint8_t  patch;
        uint8_t  prerelease;
        uint32_t commit;
        uint32_t scratch;
        std::string version;
    };


    // Create a base class with config and status as the templated types. 
    class Sys_ID_Driver : public fpga_driver::Base<sys_id_config_t, sys_id_status_t>
    {
        public:
            Sys_ID_Driver(RegisterSetInfos &regsetinfos, Logger &logger) 
            : Base<sys_id_config_t,sys_id_status_t>(regsetinfos, logger), regsetinfos_(regsetinfos) {
                csr_params_ = regsetinfos_.at("csr").get_parameters<sys_id_param_t>();
                auto *csr_map_ = new RegisterMap<sys_id_regset::sys_id_reg_t, sys_id_regset::sys_id_mem_t>(get_regset_info("csr", sys_id_regset::version));
                csr_ = csr_map_->regs();
            };

            void setIdentity(const std::string &name, const std::string &address) {
                this->name = name;
                this->address = address;
            };

            ~Sys_ID_Driver () {
                csr_.reset();
            };

            virtual void get_default_config (sys_id_config_t &c) {
                c.scratch = 0;
            };

        private:
            RegisterSetInfos regsetinfos_;
            std::shared_ptr<sys_id_regset::sys_id_reg_t> csr_ = nullptr;
            sys_id_param_t csr_params_;
            std::string name    = "";
            std::string address = "";

            virtual void configure_(const sys_id_config_t &c) {
                csr_->scratch = c.scratch;
            };

            virtual void recover_() {
                sys_id_config_t c;
                get_default_config(c);
                configure_(c);
            };

            virtual void status_(sys_id_status_t &s, bool clear = false) {
                log->info("Getting FPGA Info");
                s.bitstream                = csr_->bitstream;
                s.major                    = csr_->major;
                s.minor                    = csr_->minor;
                s.patch                    = csr_->patch;
                s.prerelease               = csr_->prerelease;
                s.commit                   = csr_->commit;
                s.scratch                  = csr_->scratch;
                s.version                  = std::to_string(s.major) + "." +
                                             std::to_string(s.minor) + "." +
                                             std::to_string(s.patch) + "-" +
                                             std::to_string(s.prerelease);
            };
    };

};

#endif

