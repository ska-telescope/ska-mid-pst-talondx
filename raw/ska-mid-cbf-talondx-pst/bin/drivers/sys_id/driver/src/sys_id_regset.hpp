
#ifndef __SYS_ID_REGSET__H__
    #define __SYS_ID_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace sys_id_regset {

        const std::string mnemonic = "sys_id";
        const std::string version = "1.0.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _bitstream_s
        {
            reg_t<std::uint32_t,is_reg,31,0> bitstream; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _bitstream_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _bitstream_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _version_s
        {
            union {
                reg_t<std::uint32_t,is_reg,7,0> prerelease; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,15,8> patch; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,23,16> minor; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,31,24> major; /*!< [ro] */
            };
        };

        template<bool is_reg=false>
        union _version_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _version_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _commit_s
        {
            reg_t<std::uint32_t,is_reg,31,0> commit; /*!< [ro] */
        };

        template<bool is_reg=false>
        union _commit_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _commit_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _scratch_s
        {
            reg_t<std::uint32_t,is_reg,31,0> scratch; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _scratch_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _scratch_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) sys_id_regs
        {
            _bitstream_u<is_reg> bitstream;
            _version_u<is_reg> version;
            _commit_u<is_reg> commit;
            _scratch_u<is_reg> scratch;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) sys_id_fields
        {
            reg_t<std::uint32_t,is_reg,31,0> bitstream; /*!< [ro] */
            union {
                reg_t<std::uint32_t,is_reg,7,0> prerelease; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,15,8> patch; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,23,16> minor; /*!< [ro] */
                reg_t<std::uint32_t,is_reg,31,24> major; /*!< [ro] */
            };
            reg_t<std::uint32_t,is_reg,31,0> commit; /*!< [ro] */
            reg_t<std::uint32_t,is_reg,31,0> scratch; /*!< [rw] */
        };

        template<bool is_reg=false>
        union sys_id_u
        {
            sys_id_regs<is_reg> reg;
            sys_id_fields<is_reg> fld;
        };

        typedef sys_id_fields<true>  sys_id_reg_t;
        typedef sys_id_fields<false> sys_id_mem_t;




        static_assert(4 == sizeof(_bitstream_s<>), "struct _bitstream_s does not have the correct size.");
        static_assert(4 == sizeof(_bitstream_u<>), "union _bitstream_u does not have the correct size.");
        static_assert(4 == sizeof(_version_s<>), "struct _version_s does not have the correct size.");
        static_assert(4 == sizeof(_version_u<>), "union _version_u does not have the correct size.");
        static_assert(4 == sizeof(_commit_s<>), "struct _commit_s does not have the correct size.");
        static_assert(4 == sizeof(_commit_u<>), "union _commit_u does not have the correct size.");
        static_assert(4 == sizeof(_scratch_s<>), "struct _scratch_s does not have the correct size.");
        static_assert(4 == sizeof(_scratch_u<>), "union _scratch_u does not have the correct size.");
        static_assert(16 == sizeof(sys_id_regs<>), "struct sys_id_regs does not have the correct size.");
        static_assert(16 == sizeof(sys_id_fields<>), "struct sys_id_fields does not have the correct size.");
        static_assert(16 == sizeof(sys_id_u<>), "union sys_id_u does not have the correct size.");
        static_assert(16 == sizeof(sys_id_reg_t), "type sys_id_reg_t does not have the correct size.");
        static_assert(16 == sizeof(sys_id_mem_t), "type sys_id_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
