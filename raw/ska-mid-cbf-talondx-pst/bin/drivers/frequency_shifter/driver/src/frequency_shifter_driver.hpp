#ifndef FREQUENCY_SHIFTER_H
#define FREQUENCY_SHIFTER_H

#include <vector>
#include <string>

#include <algorithm>
#include <stdexcept>
#include <cmath>

#include "frequency_shifter_regset.hpp"
#include "fpga_driver_base.hpp"

#define PHASES 18
#define MAX_FREQ_SHIFT 108458671

namespace fpga_driver {
    struct frequency_shifter_param_t {
    };

    struct frequency_shifter_config_t
    {
        int32_t shift_frequency;
    };

    struct frequency_shifter_status_t
    {
        int32_t shift_frequency_status;
    };

    class Frequency_Shifter_Driver : public fpga_driver::Base<frequency_shifter_config_t, frequency_shifter_status_t>
    {
        public:
        Frequency_Shifter_Driver (frequency_shifter_param_t &params, RegisterSetInfos &regset, Logger &logger)
        : Base<frequency_shifter_config_t,frequency_shifter_status_t>(regset, logger), params_(params)
        {
            log->info("Constructing...");
            RegisterMap<frequency_shifter_regset::frequency_shifter_reg_t> *reg_map = new RegisterMap<frequency_shifter_regset::frequency_shifter_reg_t>(get_regset_info("csr", frequency_shifter_regset::version));
            csr = reg_map->regs();
        };

        ~Frequency_Shifter_Driver ()
        {
            log->info("Destructor...");
            csr.reset();
        };

        void setIdentity(const std::string &name, const std::string &address)
        {
            this->name = name;
            this->address = address;
        };

        virtual void get_default_config (frequency_shifter_config_t &c)
        {
            c.shift_frequency = 0;

        };

    private:
        RegisterMap<frequency_shifter_regset::frequency_shifter_reg_t> *reg_map;
        std::shared_ptr<frequency_shifter_regset::frequency_shifter_reg_t> csr = nullptr;
        frequency_shifter_param_t params_;

        std::string name;
        std::string address;

        virtual void configure_(const frequency_shifter_config_t &c)
        {
            int32_t phinc_set = static_cast<int32_t>(c.shift_frequency);

            // Check if the phase increment is within the range: [-100 MHz to 100 MHz]
            if (abs(phinc_set) > MAX_FREQ_SHIFT)
            {
                if (phinc_set > MAX_FREQ_SHIFT)
                    phinc_set = MAX_FREQ_SHIFT;
                else
                    phinc_set = -MAX_FREQ_SHIFT;
            }

            // Write the phase_inc register for each phase; this loop writes
            // the unique phase offset for each frequency shifter core [0..17].
            // Note there is an additional write required: when phs = PHASES,
            // phase_inc = phinc_set*PHASES, which is the phase increment value
            // that gets assigned to every frequency shifter core.
            for (unsigned phs = 0; phs <= PHASES; phs++)
                csr->phase_inc = phinc_set*phs;
        };

        virtual void status_(frequency_shifter_status_t &s, bool clear = false)
        {
            s.shift_frequency_status = csr->phase_inc;
        };

        virtual void recover_()
        {

            log->info("Recovering frequency shifter configuration.");

            int32_t phinc_set = 0;
            for (unsigned phs = 0; phs <= PHASES; phs++)
                csr->phase_inc = phinc_set*phs;

        };

        virtual void deconfigure_(const frequency_shifter_config_t &c)
        {
            log->info("Deconfiguring frequency_shifter configuration.");

            recover_();

        };

        virtual void start_()
        {
            log->info("Starting frequency_shifter.");
        };

        virtual void stop_(bool force = false)
        {
            log->info("Stopping frequency_shifter.");
        };
    };
};
#endif // FREQUENCY_SHIFTER_H