
#ifndef __FREQUENCY_SHIFTER_REGSET__H__
    #define __FREQUENCY_SHIFTER_REGSET__H__
    #include <cstdint>
    #include <assert.h>
    #include <iostream>
    #include "register_types.hpp"
    using fpga_driver::reg_t;
    namespace frequency_shifter_regset {

        const std::string mnemonic = "frequency_shifter";
        const std::string version = "1.0.0";

        template<bool is_reg=false>
        struct __attribute__((__packed__)) _frequency_shifter_phase_s
        {
            reg_t<std::int32_t,is_reg,31,0> phase_inc; /*!< [rw] */
        };

        template<bool is_reg=false>
        union _frequency_shifter_phase_u
        {
            reg_t<std::uint32_t,is_reg> reg;
            _frequency_shifter_phase_s<is_reg> fld;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) frequency_shifter_regs
        {
            _frequency_shifter_phase_u<is_reg> frequency_shifter_phase;
        };

        template<bool is_reg=false>
        struct __attribute__((__packed__)) frequency_shifter_fields
        {
            reg_t<std::int32_t,is_reg,31,0> phase_inc; /*!< [rw] */
        };

        template<bool is_reg=false>
        union frequency_shifter_u
        {
            frequency_shifter_regs<is_reg> reg;
            frequency_shifter_fields<is_reg> fld;
        };

        typedef frequency_shifter_fields<true>  frequency_shifter_reg_t;
        typedef frequency_shifter_fields<false> frequency_shifter_mem_t;




        static_assert(4 == sizeof(_frequency_shifter_phase_s<>), "struct _frequency_shifter_phase_s does not have the correct size.");
        static_assert(4 == sizeof(_frequency_shifter_phase_u<>), "union _frequency_shifter_phase_u does not have the correct size.");
        static_assert(4 == sizeof(frequency_shifter_regs<>), "struct frequency_shifter_regs does not have the correct size.");
        static_assert(4 == sizeof(frequency_shifter_fields<>), "struct frequency_shifter_fields does not have the correct size.");
        static_assert(4 == sizeof(frequency_shifter_u<>), "union frequency_shifter_u does not have the correct size.");
        static_assert(4 == sizeof(frequency_shifter_reg_t), "type frequency_shifter_reg_t does not have the correct size.");
        static_assert(4 == sizeof(frequency_shifter_mem_t), "type frequency_shifter_mem_t does not have the correct size.");

    /* trailer stuff */
    }
#endif
