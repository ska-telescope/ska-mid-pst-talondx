#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "src/frequency_shifter_driver.hpp"

namespace py = pybind11;
using namespace fpga_driver;

FPGA_DRIVER_PYBIND_MODULE_DECLARATION(frequency_shifter, m) {
    py::class_<frequency_shifter_param_t>(m, "param_t")
        .def(py::init<>());

    py::class_<frequency_shifter_config_t>(m, "config_t")
        .def(py::init<>())
        .def(py::init<int32_t>(), py::arg("shift_frequency"))
        .def_readwrite("shift_frequency", &frequency_shifter_config_t::shift_frequency);

    py::class_<frequency_shifter_status_t>(m, "status_t")
        .def(py::init<>())
        .def_readwrite("shift_frequency_status", &frequency_shifter_status_t::shift_frequency_status);

    py::class_<Frequency_Shifter_Driver>(m, "driver")
        .def(py::init<frequency_shifter_param_t&, RegisterSetInfos&, Logger&>())
        .def("recover", &Frequency_Shifter_Driver::recover)
        .def("configure", &Frequency_Shifter_Driver::configure)
        .def("deconfigure", &Frequency_Shifter_Driver::deconfigure)
        .def("start", &Frequency_Shifter_Driver::start)
        .def("stop", &Frequency_Shifter_Driver::stop)
        .def("status", &Frequency_Shifter_Driver::status)
        .def("get_default_config", &Frequency_Shifter_Driver::get_default_config);
}